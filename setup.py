from setuptools import setup

setup(name='idsm',
      version='0.1',
      description='Iterant Defomable Sensorimotor Medium',
      url='http://matthewegbert.com',
      author='Matthew Egbert',
      author_email='mde@matthewegbert.com',
      license='MIT',
      packages=['idsm'],
      zip_safe=False)
