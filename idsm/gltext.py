import graphics
import FTGL
from OpenGL.GL import *
from OpenGL.GLU import *

font = FTGL.TextureFont("/usr/share/fonts/truetype/freefont/FreeSans.ttf")
#font = FTGL.BitmapFont("/usr/share/fonts/truetype/freefont/FreeSans.ttf")
#font = FTGL.PolygonFont("/usr/share/fonts/truetype/freefont/FreeSans.ttf")
#font = FTGL.OutlineFont("/usr/share/fonts/truetype/freefont/FreeSans.ttf")

font.FaceSize(32)
fscale = 1.0/graphics.window_width

def glText(text,x,y) :
    """ x and y are in -1,1 for the whole window """
    glPushMatrix()
    glColor4d(1,1,1,1)
    glTranslated(x,y,0)
    glScaled(fscale,fscale,fscale)
    font.Render(text)
    glPopMatrix()



### Local Variables: ###
### python-main-file: "main.py" ###
### End: ###
