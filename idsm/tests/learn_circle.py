from idsm import IDSM,SensorimotorDimension
from pylab import *

smds = [SensorimotorDimension('motor1',(0.0,1.0)),
        SensorimotorDimension('motor2',(0.0,1.0)),
]
idsm = IDSM(smds)
#idsm.all_nodes.randomizeNodesRandomWalk(n=5)
for pos in linspace(0,2*np.pi,4*101) :
    idsm.SMS_H[:] = [0.5+0.25*cos(pos),0.5+0.25*sin(pos)]
    idsm.iterate()

idsm.all_nodes.DtoH()
idsm.all_nodes.plot2dQuiver()
xlim(0,1)
ylim(0,1)

# ##
# idsm.all_nodes.DtoH()
# idsm.streamplot()

# ##
# # idsm.all_nodes.DtoH()
# # plot(idsm.all_nodes.H)

xlim(0,1)
ylim(0,1)
show()
