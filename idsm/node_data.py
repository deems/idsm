import matplotlib.pyplot as plt
import numpy as np
import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray
from idsm.utils import logger

class Node(object) :
    def __init__(self,loc,vel,weight,time_since_creation) :
        self.loc = loc
        self.vel = vel
        self.weight = weight
        self.time_since_creation = time_since_creation
        
class NodeData(object) :
    def __init__(self, IDSM, N_NODES, N_DIM) :
        self.IDSM = IDSM
        self.N_DIM = N_DIM
        self.N_NODES = N_NODES
        self.NODE_SIZE = (self.N_DIM + ## loc
                          self.N_DIM + ## vel
                          1 +       ## weight
                          1)        ## time since creation

        self.D = gpuarray.zeros((self.N_NODES * self.NODE_SIZE),dtype=np.float32)
        self.H = self.D.get(pagelocked=True)

        self.initializeNodeData()

    @property
    def omegas(self):
        """ returns weights scaled by function into 0..1 omega-space """
        return 2.0/(1.0 + np.exp(-self.IDSM.parameters.k_omega*self.weights)) * (self.time_since_creations >= self.IDSM.k_activation_delay)    
        
    def initializeNodeData(self) :
        self.DtoH()
        self.n_active_nodes = 0
        self.locs[:] = 0.0
        self.vels[:] = 0.0
        self.time_since_creations[:] = 0.0
        self.weights[:] = -100000000.0
        self.HtoD()

    def DtoH(self) :
        """ Pull all node data from Device to Host. """
        self.D.get(ary=self.H)
        v_size = self.N_DIM*self.N_NODES ## size of vector arrays
        s_size = self.N_NODES            ## size of scalararrays

        self.locs    = self.H[0:v_size].reshape(self.N_NODES,self.N_DIM).transpose()
        self.vels    = self.H[v_size:v_size*2].reshape(self.N_NODES,self.N_DIM).transpose()
        self.weights = self.H[v_size*2:v_size*2+s_size]
        self.time_since_creations = self.H[v_size*2+s_size:]

        self.weights_D = self.D[v_size*2:v_size*2+s_size]

    def HtoD(self) :
        self.D.set(self.H)

    def asyncDtoH(self) :
        """ Pull all node data from Device to Host. """
        self.D.get_async(ary=self.H)
        v_size = self.N_DIM*self.N_NODES ## size of vector arrays
        s_size = self.N_NODES            ## size of scalararrays

        self.locs    = self.H[0:v_size].reshape(self.N_NODES,self.N_DIM).transpose()
        self.vels    = self.H[v_size:v_size*2].reshape(self.N_NODES,self.N_DIM).transpose()
        self.weights = self.H[v_size*2:v_size*2+s_size]
        self.time_since_creations = self.H[v_size*2+s_size:]

        self.weights_D = self.D[v_size*2:v_size*2+s_size]
        
    def reset(self) :
        self.initializeNodeData()

    def __getitem__(self,ii) :
        return {
            'loc' : 
            self.H[ii*self.N_DIM:ii*self.N_DIM+self.N_DIM],
            'vel' : 
            self.H[(self.N_DIM*self.N_NODES)+ii*self.N_DIM:
                   (self.N_DIM*self.N_NODES)+ii*self.N_DIM+self.N_DIM],
            'weight' : 
            self.H[(2*self.N_DIM*self.N_NODES)+ii],
            'time_since_creation' : 
            self.H[(2*self.N_DIM*self.N_NODES)+self.N_NODES+ii*self.N_DIM],
        }

    def __setitem__(self,ii,new_node) :
        self.locs[:,ii] = new_node.loc
        self.vels[:,ii] = new_node.vel
        self.weights[ii] = new_node.weight
        self.time_since_creations[ii] = new_node.time_since_creation

    def addNode(self,node) :
        if self.n_active_nodes >= self.IDSM.N_NODES :
            logger.error('Trying to add node when node arrays are already at max capacity.')
            return False
        else :
            self[self.n_active_nodes] = node
            self.n_active_nodes += 1

    def plotValues(self) :
        plt.figure()
        nrows = 2 + self.N_DIM + self.N_DIM

        for dim in xrange(self.N_DIM) :
            plt.subplot2grid((nrows,1),(dim,0))
            plt.plot(xrange(self.N_NODES),self.locs[dim,:])
            plt.xlabel('loc (dim %d)' %(dim))

        for dim in xrange(self.N_DIM) :
            plt.subplot2grid((nrows,1),(self.N_DIM+dim,0))
            alpha = dim * self.N_NODES
            omega = (dim+1) * self.N_NODES
            plt.plot(xrange(self.N_NODES),self.vels[dim,:])
            plt.xlabel('vel (dim %d)' %(dim))

        plt.subplot2grid((nrows,1),(2*self.N_DIM+0,0))
        plt.plot(self.weights)
        plt.xlabel('weights')

        plt.subplot2grid((nrows,1),(2*self.N_DIM+1,0))
        plt.plot(self.time_since_creations)
        plt.xlabel('time since creation')
        plt.tight_layout()       

        
       
### Local Variables: ###
### python-main-file: "main.py" ###
### End: ###
