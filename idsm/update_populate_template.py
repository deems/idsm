from jinja2 import Template
import os,sys

"""This template creates four cuda kernels:

 - populateAccumulators
 - updateNodes
 - updateNodesAndPopulateAccumulators
 - updateNodesAndPopulateAccumulatorsDebug

where the third performs both of the first two, but requires only a
single kernel invocation, and the fourth maintains additional
accumulators making it possible to separate the VEL. influence of the
IDSM from the ATT. influence.

"""

####################
cuda_fns = [
{
    'fn_name' : 'updateNodesAndPopulateAccumulatorsDebug',
    'signature' : 'PPPPPPP',
    'args' : """
							 IDSM *nodes,
							 IDSMParameters *IDSMPar,
							 float *outputAccum,
							 float *familiarityAccum,
							 float *familiarityIgnoringOmegaAccum,
							 float *phiAccum,
							 float *DEBUG_outputAttractionAccum,
							 float *DEBUG_outputVelocityAccum,
							 float *SMState""",
    'update_nodes' : True,
    'populate_accum' : True,
    'generate_debugging_accumulators' : True,
},
{
'fn_name' : 'updateNodesAndPopulateAccumulators',
'signature' : 'PPPPP',
'args' : """
							 IDSM *nodes,
							 IDSMParameters *IDSMPar,
							 float *outputAccum,
							 float *familiarityAccum,
							 float *familiarityIgnoringOmegaAccum,
							 float *phiAccum,
							 float *SMState""",
    'update_nodes' : True,
    'populate_accum' : True,
    'generate_debugging_accumulators' : False,
},
{
'fn_name' : 'updateNodes',
'signature' : 'PPP',
'args' : """
							 IDSM *nodes,
							 IDSMParameters *IDSMPar,
							 float *SMState""",
    'update_nodes' : True,
    'populate_accum' : False,
},
{
'fn_name' : 'populateAccumulators',
'signature' : 'PPP',
'args' : """
							 IDSM *nodes,
							 IDSMParameters *IDSMPar,
							 float *outputAccum,
							 float *familiarityAccum,
							 float *familiarityIgnoringOmegaAccum,
							 float *phiAccum,
							 float *SMState""",
    'populate_accum' : False,
},
]

cuda_str = ''
for func in cuda_fns :
    fn = 'cuda_update_populate.cpp'
    path = os.path.dirname(__file__)
    with open(os.path.join(path,fn), 'r') as tplFile:
        tpl_string=tplFile.read()
        tplFile.close()
    
    tpl = Template(tpl_string)
    rendered_tpl = tpl.render(func)
    cuda_str += rendered_tpl
    
