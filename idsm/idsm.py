from pylab import *
import numpy as np
import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray

from .cuda_utilities.reduction.cuda_reduce import cuda_reduce
from .cuda_utilities.index_of_minimum import cuda_getIndexOfMinValue
from functools import partial

from idsm.utils import logger
import idsm.update_populate_template as update_populate_template
from idsm.node_data import NodeData

from jinja2 import Template
import os

def wrapped_euclidean_distance(p, q):
    TORUS_RADIUS = 1.0
    diff = np.abs(p - q)
    return np.linalg.norm(np.minimum(diff, TORUS_RADIUS - diff))

class SensorimotorDimension(object) :
    def __init__(self,name,limits) :
        self.name = name
        self.limits = limits

class IDSMParameters(object):
    def __init__(self) :
        NUM_PARS = 4
        # k_weight_reinforcement
        # k_weight_degradation
        # k_d
        # k_omega
        self.D = gpuarray.zeros((NUM_PARS),dtype=np.float32)
        self.H = self.D.get(pagelocked=True)

    def __str__(self):
        s = ''
        s += f'k_weight_reinforcement: {self.k_weight_reinforcement}\n'
        s += f'k_weight_degradation: {self.k_weight_degradation}\n'
        s += f'k_d: {self.k_d}\n'
        s += f'k_omega: {self.k_omega}\n'
        return s
        
    @property
    def k_weight_reinforcement(self):
        # self.D.get_async(ary=self.H)
        return self.H[0]

    @k_weight_reinforcement.setter
    def k_weight_reinforcement(self, value):
        self.H[0] = value
        self.D.set(self.H)        

    @property
    def k_weight_degradation(self):
        # self.D.get_async(ary=self.H)
        return self.H[1]

    @k_weight_degradation.setter
    def k_weight_degradation(self, value):
        self.H[1] = value
        self.D.set(self.H)        

    @property
    def k_d(self):
        # self.D.get_async(ary=self.H)
        return self.H[2]

    @k_d.setter
    def k_d(self, value):
        self.H[2] = value
        self.D.set(self.H)        

    @property
    def k_omega(self):
        # self.D.get_async(ary=self.H)
        return self.H[3]

    @k_omega.setter
    def k_omega(self, value):
        self.H[3] = value
        self.D.set(self.H)        
        
        
class IDSM(object):
    def __init__(self,sensorimotor_dimensions,DT=0.01,parameter_overrides={}) :
        # self.H_is_uptodate = False
        # self.D_is_uptodate = False

        self.sensorimotor_dimensions = sensorimotor_dimensions

        self.N_DIM = len(sensorimotor_dimensions)
        self.N_NODES = 2**14
        self.k_d = 10000.0 ## bigger is higher IDSM-resolution (more nodes per unit of SM space)
        self.k_omega = 0.0025
        self.k_activation_delay = 1.0
        self.DT = DT
        self.k_familiarity_threshold = 1.0 # less than this value, a new node will be created)

        ## parameters for deciding whether or not random motion generation will engage
        self.random_motor_activity_threshold = 1.0 
        self.random_motor_activity_gain = 20.0

        ## parameters for what the randomly generated motor activity is
        self.init_random_scale = 2.0
        self.d_random_scale = 10.0
        self.changes_in_random_direction_per_t = 2.0 

        # a discrete jump faster than this, and no new node will be created
        self.k_max_dSMS_dt = 1.0*pow(0.1*self.N_DIM,1.0/self.N_DIM)/self.DT

        self.k_weight_reinforcement = 1000.0
        self.k_weight_degradation = 5.0

        self.random_dSMS_dt = zeros(self.N_DIM)

        ## override any values passed in parameter_overrides
        vars(self).update(parameter_overrides)
        self.parameters = IDSMParameters()
        self.parameters.k_weight_reinforcement = self.k_weight_reinforcement
        self.parameters.k_weight_degradation = self.k_weight_degradation
        self.parameters.k_d = self.k_d
        self.parameters.k_omega = self.k_omega
        # print('hi:',self.parameters.k_weight_degradation)
        # quit()

        
        self.generateCUDA()
        self.initializeVariables()
        self.initializeReductionApparatus()
        self.initializeStreams()
        self.initializeFunctions()

        self.iteration = 0 ## used for history tracking

        self.phi = np.zeros(1)
        self.random_amount = np.zeros(1)
        self.random_trigger = np.zeros(1)        

    def reset(self) :
        self.SMS_D[:] *= 0.0
        self.SMS_H[:] *= 0.0
        self.prevSMS[:] *= 0.0
        self.outputAccum_D[:] *= 0.0
        self.phiAccum_D[:] *= 0.0
        self.familiarityAccum_D[:] *= 0.0
        self.familiarityIgnoringOmegaAccum_D[:] *= 0.0
        self.outputAttractionAccumDEBUG_D[:] *= 0.0
        self.outputVelocityAccumDEBUG_D[:] *= 0.0
        self.iteration = 0 ## used for history tracking
        self.weights_indices_D[:] = arange(self.N_NODES,dtype=np.int32)

        # self.outputAccum_D = gpuarray.zeros((self.N_NODES*self.N_DIM),np.float32)
        # self.phiAccum_D = gpuarray.zeros((self.N_NODES),np.float32)
        # self.familiarityAccum_D = gpuarray.zeros((self.N_NODES),np.float32)
        # self.familiarityIgnoringOmegaAccum_D = gpuarray.zeros((self.N_NODES),np.float32)
        # self.outputAttractionAccumDEBUG_D = gpuarray.zeros(self.N_NODES*self.N_DIM,np.float32)
        # self.outputVelocityAccumDEBUG_D = gpuarray.zeros(self.N_NODES*self.N_DIM,np.float32)

        # self.weights_indices_D = gpuarray.to_gpu( arange(self.N_NODES,dtype=np.int32) )

        # self.D_is_uptodate = True
        # self.H_is_uptodate = True


        self.phi[:] *= 0.0
        self.random_amount[:] *= 0.0

        self.all_nodes.initializeNodeData()

    def save(self) :
        pass

    def generateCUDA(self) :
        # print '\n=== Device attributes'
        # dev = pycuda.autoinit.device
        # print 'Name:', dev.name()
        # print 'Compute capability:', dev.compute_capability()
        # print 'Concurrent Kernels:', \
        #     bool(dev.get_attribute(cuda.device_attribute.CONCURRENT_KERNELS))

        ## open and render CUDA template
        fn = 'cuda_idsm.cpp'
        path = os.path.dirname(__file__)
        with open(os.path.join(path,fn), 'r') as tplFile:
            tpl_string=tplFile.read()
            tplFile.close()

        self.update_populate_cuda = update_populate_template.cuda_str

        tpl = Template(tpl_string)
        rendered_tpl = tpl.render(vars(self))

        # write the generated cuda to a file
        debug_string = str(rendered_tpl)
        lines = iter(str(debug_string).splitlines())
        with open(fn+'.cu','wt') as f:
            for i,l in enumerate(lines) :
                f.write("/*%3d: */%s\n" %(i+1,l))

        DEBUG = False
        if DEBUG :
            self.mod = SourceModule(rendered_tpl, no_extern_c=True,
                                    options=["-linedata","-g","-G"])
        else :
            self.mod = SourceModule(rendered_tpl, no_extern_c=True)

    def initializeFunctions(self) :
        nThreads = (32*4,1,1) # always multiple of 32
        assert(np.fmod(self.N_NODES/nThreads[0],1.0) == 0.0)
        nBlocks = (int(self.N_NODES/nThreads[0]),1)
        # print(nThreads)
        # print(nBlocks)

        ####################################
        ## creates self.updateNodes(sensorimotor_state)
        ##     updates weights according to SMState
        ##     updates TSC
        method = self.mod.get_function('updateNodes').prepare('PP')
        method = partial(method.prepared_call,nBlocks,nThreads,
                         self.all_nodes.D.gpudata,
                         self.parameters)
        setattr(IDSM,'updateNodes',method)
        ####################################

        ####################################
        ## creates self.populateAccumulators(sensorimotor_state)
        method = self.mod.get_function('populateAccumulators').prepare('PPPPPPP')
        method = partial(method.prepared_call,nBlocks,nThreads,
                         self.all_nodes.D.gpudata,
                         self.parameters.D.gpudata,
                         self.outputAccum_D.gpudata,
                         self.familiarityAccum_D.gpudata,
                         self.familiarityIgnoringOmegaAccum_D.gpudata,
                         self.phiAccum_D.gpudata,
        )
        setattr(IDSM,'populateAccumulators',method)
        ####################################

        ####################################
        ## creates self.updateNodesAndPopulateAccumulators(sensorimotor_state)
        method = self.mod.get_function('updateNodesAndPopulateAccumulators').prepare('PPPPPPP')
        method = partial(method.prepared_call,nBlocks,nThreads,
                         self.all_nodes.D.gpudata,
                         self.parameters.D.gpudata,
                         self.outputAccum_D.gpudata,
                         self.familiarityAccum_D.gpudata,
                         self.familiarityIgnoringOmegaAccum_D.gpudata,
                         self.phiAccum_D.gpudata,
        )
        setattr(IDSM,'updateNodesAndPopulateAccumulators',method)
        ####################################

        ####################################
        ## creates         self.setNodeData(self.lowestWeightIndex,nodedata_as_single_contiguous_array)
        method = self.mod.get_function('setNodeData').prepare('PiP')
        method = partial(method.prepared_call,(1,1,1),(1,1,1),
                         self.all_nodes.D.gpudata,
        )
        setattr(IDSM,'setNodeData',method)
        ####################################

    def initializeVariables(self) :
        self.all_nodes = NodeData(self,self.N_NODES,self.N_DIM)

        self.SMS_D = gpuarray.zeros(self.N_DIM,dtype=np.float32)
        self.SMS_H = self.SMS_D.get(pagelocked=True)
        self.prevSMS = array(self.SMS_H)

        self.outputAccum_D = gpuarray.zeros((self.N_NODES*self.N_DIM),np.float32)
        self.phiAccum_D = gpuarray.zeros((self.N_NODES),np.float32)
        self.familiarityAccum_D = gpuarray.zeros((self.N_NODES),np.float32)
        self.familiarityIgnoringOmegaAccum_D = gpuarray.zeros((self.N_NODES),np.float32)
        self.outputAttractionAccumDEBUG_D = gpuarray.zeros(self.N_NODES*self.N_DIM,np.float32)
        self.outputVelocityAccumDEBUG_D = gpuarray.zeros(self.N_NODES*self.N_DIM,np.float32)

        self.weights_indices_D = gpuarray.to_gpu( arange(self.N_NODES,dtype=np.int32) )

        # self.D_is_uptodate = True
        # self.H_is_uptodate = True


    def initializeReductionApparatus(self) :
        self.reductionArrays = [self.familiarityAccum_D,
                                self.familiarityIgnoringOmegaAccum_D,
                                self.phiAccum_D]
        for d in range(self.N_DIM) :
            divisions = linspace(0,self.N_NODES*self.N_DIM,self.N_DIM+1)
            start = int(divisions[d])
            stop = int(divisions[d+1])
            self.reductionArrays.append( self.outputAccum_D[start:stop] )

    def initializeStreams(self) :
        pass
        #self.memory_transfer_stream = cuda.Stream()
        #self.reductionStreams = [cuda.Stream() for _ in xrange(len(self.reductionArrays))]

    def reduceCudaArrays(self) :
        cuda_reduce(self.reductionArrays,
                    self.N_NODES)#,streams=self.reductionStreams)

    def getSMS(self) :
        return self.SMS_D.get()

    def iterate(self) :
        self.SMS_D.set(self.SMS_H) # this *may* be unnecessary
        self.familiarity = self.familiarityAccum_D[:1].get()
        self.phi = self.phiAccum_D[:1].get()
        self.dSMS = self.dSMS_dt()
        self.random_trigger[:] = self.phi#self.familiarityIgnoringOmegaAccum_D[:1].get()
        
        ## linear segments random motor activity
        if self.iteration == 0 or (np.random.rand() < self.DT *
                                   self.changes_in_random_direction_per_t) :
            self.random_dSMS_dt = self.init_random_scale*np.random.randn(self.N_DIM)
            self.random_ds = self.d_random_scale*np.random.randn(self.N_DIM)
        else :
            self.random_dSMS_dt += self.random_ds * self.DT        
            
        #### RANDOM INFLUENCE FOR LOW FAMILIARITY SITUATIONS
        fam = self.random_trigger
        self.random_amount[:] = 1.0/(1.0+exp(
            self.random_motor_activity_gain*(fam-self.random_motor_activity_threshold)))
        self.dSMS = self.random_amount*self.random_dSMS_dt + (1.0-self.random_amount)*self.dSMS
        #### END RANDOM INFLUENCE FOR LOW FAMILIARITY SITUATIONS

        #print('IDSM influence: %f,%f \t(phi:%e)' %(dSMS[0],dSMS[1],fam))
        self.addNodeIfAppropriate()

        # ## update SMS
        self.prevSMS = array(self.SMS_H)
        self.SMS_H += self.dSMS*self.DT

        self.iteration += 1

    def addNodeIfAppropriate(self) :
        #self.memory_transfer_stream.synchronize()
        if self.familiarity < self.k_familiarity_threshold :            
            #print('adding node:',self.iteration,self.lowestWeightIndex)
            ## if the density of nodes is sufficiently low, we should
            ## make a new one.

            ## the rate of change in SMS, as estimated as the
            ## difference between now and the end of the last
            ## iteration
            dSMdt = (self.SMS_H-self.prevSMS) / self.DT
            if np.linalg.norm(dSMdt) < self.k_max_dSMS_dt :
                ## dSMdt is not too big, we'll add a new node
                new_node_weight = 1000.0
                npdata = array(list(self.SMS_H)+list(dSMdt)+[new_node_weight,self.DT],dtype=np.float32)
                #print('phi: %f\tfam: %f\tvel:%f %f' %(self.phi,self.familiarity,npdata[2],npdata[3]))
                nd = gpuarray.to_gpu( array(npdata,dtype=np.float32) )
                self.setNodeData(self.lowestWeightIndex,nd.gpudata)

    def sample_dSMS_dt(self,sm_state=None) :
        """ Gets the output of the IDSM without modifying node weights etc."""
        out = self.dSMS_dt(sm_state,do_not_update_nodes=True)
        return out

    def dSMS_dt(self,sm_state=None,do_not_update_nodes=False) :
        """Calculates and returns the vector representing change in SM-state
        suggested by the IDSM for the specified sm_state. If no
        sm_state is passed, then self.sms_D is used instead (this is
        the typical case -- ie when self.iterate() is called.

        """
        if sm_state != None :
            ## we are using the argument-specified SMS
            sm_state = array(sm_state,dtype=np.float32)
            sms_D = gpuarray.to_gpu(sm_state)
            sms = sms_D
        else :
            sms = self.SMS_D

        if do_not_update_nodes :
            self.populateAccumulators(sms.gpudata)
        else :
            self.updateNodesAndPopulateAccumulators(sms.gpudata)

            #print(self.familiarityAccum_D)

        ## perform reductions
        self.lowestWeightedNodeIndex_D = cuda_getIndexOfMinValue(self.all_nodes.weights_D,
                                                                 self.weights_indices_D)
        self.lowestWeightIndex = int(self.lowestWeightedNodeIndex_D.get()['index'])
        self.reduceCudaArrays()

        out = self.outputAccum_D.get()
        d = out[0:self.N_DIM*self.N_NODES:self.N_NODES]

        self.phi = self.phiAccum_D[:1].get()
        if self.phi > 0.0 :
            d /= self.phi
            # print(self.phi,d,d/self.phi)
        else :
            d*=0.0
            # print('zero')
        return d

    def set_node_data(self, node_data) :
        self.all_nodes.reset()
        self.all_nodes.H[:] = node_data
        self.all_nodes.HtoD()
        self.iterate()

    def streamplot(self,horizontal_dim_i=0,vertical_dim_i=1,
                   xres=None,yres=None,**kwargs) :
        self.all_nodes.DtoH()

        res = 11

        if xres==None :
            xres = linspace(0,1,
                            res,dtype=np.float32)
        if yres==None :
            yres = linspace(0,1,
                            res,dtype=np.float32)
        mesh = meshgrid(xres,yres)
        delta = apply_along_axis(self.sample_dSMS_dt,0,mesh)

        lx,hx = self.sensorimotor_dimensions[0].limits
        ly,hy = self.sensorimotor_dimensions[1].limits

        x_i = horizontal_dim_i
        y_i = vertical_dim_i
        #speed = 0.01+np.sqrt(delta[x_i]**2 + delta[y_i]**2)
        speed = 0.01+0.5*np.sqrt(delta[x_i]**2 + delta[y_i]**2)
        streamplot(mesh[x_i]*(hx-lx)+lx,
                   mesh[y_i]*(hy-ly)+ly,
                   delta[x_i]*(hx-lx)+lx,
                   0.0*delta[y_i]*(hy-ly)+ly,
                   density=1.0,linewidth=speed,
                   color='k',**kwargs)
        xlim(lx,hx)
        ylim(ly,hy)
        xlabel(self.sensorimotor_dimensions[0].name)
        xlabel(self.sensorimotor_dimensions[1].name)

    def plotNodeData(self) :
        self.all_nodes.plotValues()

    def getStateOfAllNodes(self) :
        self.all_nodes.DtoH()
        return self.all_nodes

    def getNodeClosestToSMS(self):
        ## this mask selects only activated nodes
        mask = np.logical_and(self.all_nodes.weights > 0.0,
                              self.all_nodes.time_since_creations > 0.0)
        active_node_positions = self.all_nodes.locs[:,mask]

        ## the indices of active_node_positions is not the actual
        ## node indices (because it is only showing those that are active)
        ## the following array will be their actual indices
        active_node_indices = np.arange(self.all_nodes.N_NODES)[mask] 
        
        n_active_nodes = np.shape(active_node_positions)[1]

        if n_active_nodes < 2 :
            return None

        from scipy.spatial import distance

        def closest_node(node, nodes):
            closest_index = distance.cdist(node, nodes, metric=wrapped_euclidean_distance).argmin()
            return closest_index, nodes[closest_index]

        cur_sm_pos = np.expand_dims(self.SMS_H,1).T

        # print(f'#active nodes: {n_active_nodes}')
        # print(f'active node positions: {active_node_positions.T}')
        small_index, closest_node_position = closest_node(cur_sm_pos,active_node_positions.T)

        actual_index = active_node_indices[small_index]
        # print(f'{cur_sm_pos} is close to n{small_index}'+
        #       f'{active_node_positions[:,small_index]}')
        # print(f'{cur_sm_pos} is close to N{actual_index}'+
        #       f'{self.all_nodes.locs[:,actual_index]}')

        return actual_index
    
### Local Variables: ###
### python-main-file: "main.py" ###
### End: ###
