#!/usr/bin/env python
from pylab import *

import pycuda.gpuarray as gpuarray
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule

preamble = """
struct index_of_value_collector
{
    float value;
    uint index;

    __device__
    index_of_value_collector()
    { }

    __device__
    index_of_value_collector(float cmin, uint cindex)
    : value(cmin), index(cindex)
    { }

    __device__ index_of_value_collector(index_of_value_collector const &src)
    : value(src.value), index(src.index)
    { }

    __device__ index_of_value_collector(index_of_value_collector const volatile &src)
    : value(src.value), index(src.index)
    { }

    // assignment operator
    __device__ index_of_value_collector volatile &operator=(
        index_of_value_collector const &src) volatile
    {
        value = src.value;
        index = src.index;
        return *this;
    }
};

__device__
index_of_value_collector agg_mmc(index_of_value_collector a, 
                                 index_of_value_collector b)
{
    uint index = a.index;
    float value = a.value;
    if(b.value < a.value) {
        index = b.index;
        value = b.value;
    }
    return index_of_value_collector(        
        value,
        index);
}
"""
mmc_dtype = np.dtype([("value", np.float32), ("index", np.int32)])

from pycuda.tools import register_dtype
register_dtype(mmc_dtype, "index_of_value_collector")

from pycuda.reduction import ReductionKernel
INFINITY_LITERAL = "__int_as_float(0x7f800000)"
reduction = ReductionKernel(mmc_dtype,
                            neutral="index_of_value_collector("+INFINITY_LITERAL+", 0)",
                            reduce_expr="agg_mmc(a, b)",
                            map_expr="index_of_value_collector(x[i], indices[i])",
                            arguments="float *x, int *indices", preamble=preamble,
                            name='index_of_minimum_value_reduction')#,options=['-std=c++98','--x c++'])

def cuda_getIndexOfMinValue(values,indices,stream=None) :
    """Takes two gpu arrays as arguments, and returns a gpu array with two
    values: the min value and the index to that value."""
    return reduction(values,indices,stream=stream)

if __name__ == '__main__' :
    from pycuda.curandom import rand as curand
    a_gpu = curand((20000,), dtype=np.float32)
    a = a_gpu.get()

    a_indices = gpuarray.to_gpu(arange(len(a),dtype=np.int32))
    
    minmax = reduction(a_gpu,a_indices).get()
    print( reduction(a_gpu,a_indices).get() )
    print('correctIndexOfMinItem',list(a).index(a.min()))
    print('index',minmax["index"])
    print('correct value',np.min(a))
    print('value',minmax["value"])
