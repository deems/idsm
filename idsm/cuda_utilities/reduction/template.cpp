extern "C" {

  __global__ void reduce(float *g_idata, float *g_odata, unsigned int n) {
    extern __shared__ float sdata[];

    //// each thread loads two elements from global to shared mem
    // and sums them in the first step 
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*(blockDim.x*2) + threadIdx.x;

    // makes sure these elements are within bounds
    float firstSum = (i < n) ? g_idata[i] : 0;
    if (i + blockDim.x < n)
      firstSum += g_idata[i+blockDim.x];

    sdata[tid] = firstSum;
    __syncthreads();
    //// done with first summation

    /*  for (unsigned int s=blockDim.x/2; s>0; s>>=1) {
	if (tid < s) {
	sdata[tid] += sdata[tid + s];
	}
	__syncthreads();
	}*/

    for (unsigned int s=blockDim.x/2; s>32; s>>=1) {
      if (tid < s) {
	sdata[tid] += sdata[tid + s];
      }
      __syncthreads();
    }

    if (tid < 32) {
      volatile float *smem = sdata;

      if(blockDim.x >= 64)
	smem[tid] += smem[tid + 32];
      if(blockDim.x >= 32)
	smem[tid] += smem[tid + 16];
      if(blockDim.x >= 16)
	smem[tid] += smem[tid + 8];
      if(blockDim.x >= 8)
	smem[tid] += smem[tid + 4];
      if(blockDim.x >= 4)
	smem[tid] += smem[tid + 2];
      if(blockDim.x >= 2)
	smem[tid] += smem[tid + 1];
    }

    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = sdata[0];
  }

}
