#! /usr/bin/env python
from pylab import *
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule

import os
import inspect
from jinja2 import Template
import fabulous.color

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)

from multiprocessing import Process

pars = {
'max_threads' : 1024,
'max_blocks' : 128,
# 'max_threads' : 16,
# 'max_blocks' : 64,
}


## open and render CUDA template
path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) # script directory

fn = os.path.join(path,'template.cpp')
with open(fn, 'r') as tplFile:
    tpl_string=tplFile.read()
    tplFile.close()

tpl = Template(tpl_string)
rendered_tpl = tpl.render(pars)

# write the generated cuda to a file
debug_string = str(rendered_tpl)
lines = iter(str(debug_string).splitlines())
with open(fn+'.cu','wt') as f:
    for i,l in enumerate(lines) :
        f.write("/*%3d: */%s\n" %(i+1,l))

DEBUG = False
if DEBUG :
    mod = SourceModule(rendered_tpl, no_extern_c=True,
                            options=["-linedata","-g","-G"])
else :
    mod = SourceModule(rendered_tpl, no_extern_c=True)

# mod = SourceModule(modStr,no_extern_c=False) 
kernel = mod.get_function("reduce")
kernel.prepare("PPi")

def nextPow2(x) :
    x = int(x)
    x -= 1
    x |= x >> 1
    x |= x >> 2
    x |= x >> 4
    x |= x >> 8
    x |= x >> 16
    return x+1

def shapes(n) :
    if n < pars['max_threads'] * 2:
        threads = nextPow2((n + 1)// 2)
    else :
        threads = pars['max_threads']
    blocks = (n + (threads * 2 - 1)) // (threads * 2);
    return threads,blocks

def cuda_reduce(in_D,n,streams=None) :
    # print(f'cuda_reduce {in_D}, {n}')
    if streams == None :
        streams = [None]*len(in_D)
    threads,blocks = shapes(n)
    threads = int(threads)
    blocks = int(blocks)

    while n > 1:
        for index,i in enumerate(in_D) :
            # print(n)
            #### THESE DEBUGGING LOGS SLOW THINGS DOWN A LOT!!!
            #### KEEP THEM COMMENTED OUT UNLESS DEBUGGING!
            # logger.info("threads: %d\t blocks: %d n: %d" %(threads,blocks,n))
            # logger.debug(fabulous.color.blue('\nBefore reduce: %s' %(i.get())))            

            # print(blocks,threads,n)
            kernel.prepared_async_call((blocks,1,1),
                                       (threads,1,1),
                                       streams[index],
                                       i.gpudata,
                                       i.gpudata, 
                                       np.int32(n),
                                       shared_size=threads*4*2 # bytes??
            )
            n=int(n)
            # print(n)
            # logger.debug(fabulous.color.cyan('\nAfter reduce: %s' %(i.get())))

            # # n_to_copy = (n + (threads*2-1)) / (threads*2)
            # # drv.memcpy_dtod_async(i.gpudata,o.gpudata, #dest,src
            # #                       n_to_copy, stream=streams[index])


        n = int( (n + (threads*2-1)) // (threads*2) );
        threads,blocks = shapes(n)
        # print('asdf',n)


if __name__ == '__main__' :
    print(' ')
    from tests import *

    # for _ in xrange(10) :
    #     for testSize in [64, 128, 256, 512, 1024, 4096*4, 4096*8]: 
    #         test(testSize)
    #         print('\n----------\n')

    profileNVVP()

    # # test offset idea
    # testOffset(32,0,16)
    # testOffset(32,16,32)
    # testOffset(32,20,32)

    ## test simultaneous operation on different subsets of an array
    #testConcurrent(64,(0,32),(32,64))
    
