import numpy as np
from cuda_reduce import *
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule


def test(n) :
    print('Summing array of length %d.'%(n))
    #in_D = gpuarray.to_gpu( np.random.randn(n).astype(np.float32) )
    in_D = gpuarray.to_gpu( np.ones(n).astype(np.float32) )

    correctSum = sum(in_D.get())

    cuda_reduce([in_D],n)

    cudaSum = in_D.get()[0]
    error = correctSum - cudaSum 
    if abs(error) < 0.1 :
        print(fabulous.color.green('Test of size %d succeeded in resolving a sum of %f. (error of %f)' %(n,cudaSum,error)))
    else :
        print(fabulous.color.red('Test of size %d failed. Correct sum: %f \t Cuda sum: %f' %(n,correctSum,cudaSum)))

def profileNVVP() :    
    n = 2**14
    in_D = gpuarray.to_gpu( np.ones(n).astype(np.float32) )

    for _ in xrange(10) :
        cuda_reduce([in_D],n)


def testOffset(n,start_pos,stop_pos):
    print('Summing array of length %d from %d to %d. '%(n,start_pos,stop_pos))

    #in_D = gpuarray.to_gpu( np.random.randn(n).astype(np.float32) )
    in_D = gpuarray.to_gpu( np.ones(n).astype(np.float32) )

    correctSum = sum(in_D.get()[start_pos:stop_pos])
    cuda_reduce([in_D[start_pos:stop_pos]],[out_D[start_pos:stop_pos]],stop_pos-start_pos)

    cudaSum = out_D.get()[start_pos]
    error = correctSum - cudaSum 
    if abs(error) < 0.1 :
        print(fabulous.color.green('Subset test succeeded in resolving a sum of %f. (error of %f)' %(cudaSum,error)))
    else :
        print(fabulous.color.red('Subset test failed. Correct sum: %f \t Cuda sum: %f' %(correctSum,cudaSum)))

def testConcurrent(n,zero,one):
    (start0,stop0) = zero
    (start1,stop1) = one
    print('Concurrent test. Length of subdivs is %d.' %(stop0-start0))

    #in_D = gpuarray.to_gpu( np.random.randn(n).astype(np.float32) )
    #in_D = gpuarray.to_gpu( np.ones(n).astype(np.float32) )
    in_D = gpuarray.to_gpu( np.arange(n).astype(np.float32) )

    correctSums = [sum(in_D.get()[start0:stop0]),
                   sum(in_D.get()[start1:stop1])]
    
    in_arrays = [in_D[start0:stop0],
                 in_D[start1:stop1]]

    cuda_reduce(in_arrays,stop0-start0)

    cudaSums = [in_D.get()[start0],in_D.get()[start1]]
    print('correct',correctSums)
    print('cuda   ',cudaSums)
    # error = correctSum - cudaSum 
    # if abs(error) < 0.1 :
    #     print(fabulous.color.green('Subset test succeeded in resolving a sum of %f. (error of %f)' %(cudaSum,error)))
    # else :
    #     print(fabulous.color.red('Subset test failed. Correct sum: %f \t Cuda sum: %f' %(correctSum,cudaSum)))

test(2**14)
