    # def randomizeNodesRandomWalk(self,n=100,step_size=0.05,vel_amplitude=1.0,form="") :
    #     newRandomNodeIndex = 0
    #     stepSize = step_size
    #     numPaths = n
    #     pathLength = 50

    #     for pathID in xrange(numPaths) :
    #         pos = np.random.rand(self.N_DIM)
    #         for step in xrange(pathLength) :
    #             delta = (np.random.rand(self.N_DIM)-0.5)*2.0
    #             ## reverse direction for any delta that takes the
    #             ## system out of the box
    #             for d in xrange(self.N_DIM) :
    #                 postDelta = delta[d]*stepSize + pos[d]
    #                 if postDelta < 0.0 or postDelta > 1.0 :
    #                     delta[d] *= -1
    #             weight = np.random.randn()*1000.0 - 500.0

    #             if self.addNode(Node(pos,delta*vel_amplitude,weight,self.IDSM.k_activation_delay)) == False :
    #                 self.HtoD()
    #                 return
                
    #             newRandomNodeIndex+=1
    #             pos = pos+delta*stepSize
    #     self.HtoD()

    # def randomizeNodesRandomDirectionalWalk(self,n=100,step_size=0.05,vel_amplitude=1.0,form="") :
    #     newRandomNodeIndex = 0
    #     stepSize = step_size
    #     numPaths = n
    #     pathLength = 50

    #     direction = np.random.rand()*2.0*np.pi
    #     for pathID in xrange(numPaths) :
    #         pos = np.random.rand(self.N_DIM)
    #         for step in xrange(pathLength) :
    #             direction += np.random.randn()*0.5
    #             direction = direction % (2.0*np.pi)
    #             length = np.array(np.random.rand()*step_size*vel_amplitude,dtype=np.float32)
    #             delta = np.array([length*np.cos(direction),length*np.sin(direction)],dtype=np.float32)
    #             ## reverse direction for any delta that takes the
    #             ## system out of the box
    #             for d in xrange(self.N_DIM) :
    #                 postDelta = delta[d]*stepSize + pos[d]
    #                 if postDelta < 0.0 or postDelta > 1.0 :
    #                     delta[d] *= -1
    #                     direction = np.arctan2(delta[1],delta[0])
    #             weight = np.random.randn()*1000.0 - 500.0

    #             if self.addNode(Node(pos,delta*vel_amplitude,weight,self.IDSM.k_activation_delay)) == False :
    #                 self.HtoD()
    #                 return
                
    #             newRandomNodeIndex+=1
    #             pos = pos+delta*stepSize
    #     self.HtoD()
