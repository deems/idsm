#include <curand_kernel.h>
#include <stdio.h>

const float DT = {{DT}};
const int N_NODES = {{N_NODES}};
const int N_DIM = {{N_DIM}};

const float k_activation_delay = {{k_activation_delay}};
// const float k_d     = {{k_d}};     //SM_DISTANCE_CONSTANT
// const float k_omega = {{k_omega}}; //OMEGA_KW
// const float k_weight_reinforcement = {{k_weight_reinforcement}};
// const float k_weight_degradation = {{k_weight_degradation}};


extern "C" {

  struct IDSM{
    float locs[N_NODES][N_DIM];
    float vels[N_NODES][N_DIM];
    float weights[N_NODES];
    float time_since_creations[N_NODES];
  };

  struct IDSMParameters{
    // As currently implemented these should ONLY be changed by the
    // host (not on device). To change that, you will need to edit
    // the getter functions of these properties in idsm.py
    float k_weight_reinforcement;
    float k_weight_degradation;
    float k_d;
    float k_omega;
  };

  
  //// SIGMOIDAL WEIGHT TRANSFORM FUNCTION
  // \(\omega(N_w) = \frac{2}{1+\exp(-k_wN_w)}\) -- Equation (2) [1]
  __device__ float omega(float k_omega, float weight) {
    //return 2.0/(1.0 + __expf(-k_omega*(weight))); // PRE-2020 weight function
    return weight>0.0;//2.0/(1.0 + __expf(-k_omega*(weight)));
  }

  // wrapper to allow call from host
  __global__ void omega__G(float k_omega, float weight, float *output) {
    output[0] = omega(k_omega, weight);
  }

  // // DISTANCE FUNCTION
  // // \(d(\vec{x},\vec{y}) = \frac{2}{1+\exp(k_d||\vec{x}-\vec{y}||^2)}\) -- Equation (3) [1]
  // __device__ float d(float *x, float *y) {
  //   float distSquared = 0.0;
  //   for(int i=0;i<N_DIM;i++) {
  //     distSquared += (x[i] - y[i])*(x[i] - y[i]);
  //   }
  //   return 2.0 / (1.0 + __expf(k_d*distSquared));
  // }

  //// PERIODIC DISTANCE FUNCTION
  __device__ float d(float k_d, float *x, float *y) {
    float distSquared = 0.0;
    for(int i=0;i<N_DIM;i++) {
      const float x_size = 1.0;
      float dx = x[i] - y[i]; 
      dx = dx - round(dx / x_size) * x_size;
      distSquared += dx*dx;
    }
    return 2.0 / (1.0 + __expf(k_d*distSquared));
  }

  

  // wrapper to allow call from host
  __global__ void d__G(float k_d, float *x, float *y, float *output) {
    output[0] = d(k_d, x,y);
  }


  /**
   * Given the current SM-state (in normalized coordinates) and the
   * weight and location of a given node, this function calculates the
   * attraction vector as described in Equation (7) of [1]. Note that
   * in [1] there is an error in the specification of \(\Gamma\),
   * which is corrected in the Corrigendum [see reference 2].  
   * 
   * \(A(\vec{x}) = \sum_N \omega(N_w) \cdot d(N_p,\vec{x}) \cdot
   \Gamma(N_p-\vec{x},N_v)^\mu)\) -- Equation (7) [1]
   *
   * The correct expression of \(\Gamma\) is:
   *
   * \(\Gamma(\vec{a},N_v) = a - (a \cdot \frac{N_v}{||N_v||}) \frac{N_v}{||N_v||}\)
   *
   * Note also that at this stage there is no distinctuion made
   * between motor and sensory dimensions -- i.e. the attraction
   * vector is calculated in all dimensions, but only changes in the
   * motor dimensions are updated (or more accurately put: not
   * overridden by sensory input).
   */
  __device__ void attraction(IDSMParameters *IDSMPar,
			     float *SMState,
			     float *nodeLoc,
			     float *nodeVel,
			     float *a) { // the "returned" value
    // vector from current position to nodeLoc (a.k.a. 'a' a.k.a. Np-x)
    for(int dim=0;dim<N_DIM;dim++) {
      a[dim] = (nodeLoc[dim] - SMState[dim]);
    }

    //// Remove element of attraction that is parallel to Node's
    //// velocity vector

    // vNormed = \( \frac{N_v}{||N_v||} \)
    float vMag=0.0;
    for(int dim=0;dim<N_DIM;dim++) {
      vMag += nodeVel[dim]*nodeVel[dim];
    }
    vMag = 0.0001+sqrt(vMag); // small value is added to prevent a div by zero
    float vNormed[N_DIM];
    for(int dim=0;dim<N_DIM;dim++) {
      vNormed[dim] = nodeVel[dim] / vMag;
    }

    // mag =  \(a \cdot \frac{N_v}{||N_v||}  \)
    float mag = 0.0;
    for(int dim=0;dim<N_DIM;dim++) {
      mag += a[dim]*vNormed[dim];
    }

    // subtract scaled components
    for(int dim=0;dim<N_DIM;dim++) {
      // the final values in a[dim] are: \( a - (a \cdot \frac{N_v}{||N_v||})\frac{N_v}{||N_v||} \)
      a[dim] -= vNormed[dim]*mag;
    }

    // // experimenting with having much weaker attraction
    // for(int dim=0;dim<N_DIM;dim++) {
    //   a[dim] *= 0.5;
    // }
  }

  __global__ void setNodeData(IDSM *nodes, int node_i, float *nodeData) {
    for(int i=0;i<N_DIM;i++) {
      nodes->locs[node_i][i] = nodeData[i];
    }
    for(int i=0;i<N_DIM;i++) {
      nodes->vels[node_i][i] = nodeData[i+N_DIM];
    }
    nodes->weights[node_i] = nodeData[N_DIM*2];
    nodes->time_since_creations[node_i] = nodeData[N_DIM*2+1];
  }

 
  {{update_populate_cuda}} 

}

    // printf("%d:: %f\n",nid, nodes[nid].loc[0]);


// [1] Egbert & Barandiaran (2015) Modelling Habits as Self-Sustaining
//     Patterns of Sensorimotor Behaviour. Front. Hum. Neurosci., 08
//     August 2014 | http://dx.doi.org/10.3389/fnhum.2014.00590
//     http://journal.frontiersin.org/article/10.3389/fnhum.2014.00590/abstract
//
// [2] Corrigendum to [1]. 
//     http://journal.frontiersin.org/article/10.3389/fnhum.2015.00209/full
