  /**
   * Given the current sensorimotor state, and the IDSM, this function
   * populates three accumulators with the data necessary to compute
   * 
   *   (1) the combined attraction and velocity influence of all
   *       active nodes in the IDSM.
   *
   *   (2) the "familiarity" i.e. the node-density of the current
   *       sensorimotor state. This takes into account nodes that have
   *       been created, but are not yet active, and is used to
   *       determine whether or not a new node should be created at
   *       the current sensorimotor state.
   * 
   *   (3) the scaling factor, \(\phi\), which is used to attenuate
   *       the overall influence of nodes so that their influence is
   *       an average, i.e. not cumulative. This only takes into
   *       account nodes that have been activated, and for this reason
   *       is kept separate from the familiarity measure (2).
   *
   * The accumulators are later reduced (in a separate kernel) to
   * vector, scalar and scalar values respectively.
   *
   * These functions are generated from 
   */


__global__ void {{fn_name}}({{args}}
							 ) {
    uint nid = blockIdx.x * blockDim.x + threadIdx.x; // thread ID
    //// store the current node's data locally
    float nodeLoc[N_DIM];
    for(int dim=0;dim<N_DIM;dim++) {
      nodeLoc[dim] = nodes->locs[nid][dim];
    }

    float nodeTSC = nodes->time_since_creations[nid];
    float smProximity = d(IDSMPar->k_d,SMState,nodeLoc);

    {% if update_nodes %}
    // ======= UPDATE NODES ==========
    bool nodeExists = (nodeTSC > 0.0); 

    //// update weight and time since created (TSC) (but only if node
    //// exists). 
    // \(\frac{dN_w}{dt} = -k_\downarrow + k_\uparrow \cdot d(N_p,\vec{x})\) -- Eqs (4) and (5) in [1]
    nodes->weights[nid] += (-DT*IDSMPar->k_weight_degradation + IDSMPar->k_weight_reinforcement*smProximity*DT) * nodeExists;
    nodes->weights[nid] = min(nodes->weights[nid],5000.0);

    ///// Mar 30 2020
    // Remove nodes when weight falls quite low
    //float still_alive = (float)(nodes->weights[nid] > 0);
    if(nodes->weights[nid] < 0) {
      //printf("a node is getting removed: %d",nid);
      nodes->time_since_creations[nid] = 0.0;
      for(int dim=0;dim<N_DIM;dim++) {
	nodes->locs[nid][dim] = 0.0;
      }
      nodes->weights[nid] = -5000.0;
    }
    
    ///// END Mar 30 2020

    // update time since created (only if node exists).
    nodes->time_since_creations[nid] += DT * nodeExists;
    // ======= END UPDATE NODES ==========
    {% endif %}

    {% if populate_accum %}
    // ======= POPULATE ACCUM ==========
    float nodeVel[N_DIM];
    for(int dim=0;dim<N_DIM;dim++) {
      nodeVel[dim] = nodes->vels[nid][dim];
    }
    float nodeWeight = nodes->weights[nid];
    bool nodeIsActive = (nodeTSC >= k_activation_delay);

    //////////////////////////////////////////////////////////////////
    // Calculate attraction influence (velocity is already in nodeVel)
    float attractionVector[N_DIM];
    attraction(IDSMPar,SMState,nodeLoc,nodeVel,attractionVector);

    //// the influence of nodes falls off with the non-linear fn of
    // distance between the node and the SMState (1st term) and the
    // sigmoidal fn of weight of the node (2nd term).
    float attenuation = smProximity * omega(IDSMPar->k_omega,nodeWeight);

    // familiarity (which determines when new nodes are to be added)
    // is infl. by both activated /and/ disactivated nodes
    familiarityAccum[nid] = attenuation;

    //bool nodeExists = (nodeTSC > 0.0);
    familiarityIgnoringOmegaAccum[nid] = smProximity*nodeIsActive;

    // but phi-based normalization of node influence is only influnced
    // by activated nodes (see Eqs (6) and (9) in [1]).
    float combinedScaling = attenuation * nodeIsActive;
    phiAccum[nid] = combinedScaling;

    // calculate the the output of the network
    // (pre-phi-based-normalization, i.e. everything inside the sum in
    // Eq (10) of [1])
    for(int dim=0;dim<N_DIM;dim++) {
      //// the output accumulator takes the format
      //// [x_0, x_1 .. x_numNodes, y_0, y1 ... y_numNodes ....... ]
      int oID = dim*N_NODES + nid;

      float attractionInfl = attractionVector[dim] / DT;
      float velocityInfl = nodeVel[dim];

      // // main output accumulator
      outputAccum[oID] = combinedScaling * (velocityInfl + attractionInfl) * smProximity; // additional smProximity term added to make forces fall off with distance

      {% if generate_debugging_accumulators %}
      // -------- GENERATE DEBUGGING ACCUMULATORS
      // accumulators only used for debugging / visualisation of
      // different influences
      DEBUG_outputAttractionAccum[oID] = attractionInfl;
      DEBUG_outputVelocityAccum[oID] = velocityInfl;
      // -------- END GENERATE DEBUGGING ACCUMULATORS
      {% endif %}
    }      
    // ======= END POPULATE ACCUM ==========    
    {% endif %}
}
    

