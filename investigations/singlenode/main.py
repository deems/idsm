import numpy as np
from idsm.utils import logger
from idsm.node_data import Node
from idsm import SensorimotorDimension, IDSM
import pylab as pl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.mlab import bivariate_normal


smds = [SensorimotorDimension('m',(0.0,1.0)),
        SensorimotorDimension('s',(0.0,1.0))]

idsm = IDSM(smds,DT=0.01,
            parameter_overrides={'N_NODES':2**(8),
                                 'k_d':100.0, # bigger is higher IDSM-resolution (more nodes per unit of SM space)
                                 #'k_omega':0.025,
                                 'k_weight_degradation' : 5.0,#5.0*2.0,
                                 'init_random_scale' : 2.0,
                                 'd_random_scale' : 10.0,
                                 'changes_in_random_direction_per_t' : 2.0,
            })


#Node(loc,vel,weight,time_since_creation)
angle = 0.25*np.pi
vel_scale = 10.0
node = Node([0.5,0.5],
            [vel_scale*np.cos(angle),
             vel_scale*np.sin(angle)],1.0,1.0)
idsm.all_nodes.addNode(node)

# node = Node([0.55,0.55],
#             [vel_scale*np.cos(angle),
#              vel_scale*np.sin(angle)],1.0,1.0)
# idsm.all_nodes.addNode(node)

idsm.all_nodes.HtoD()
# idsm.plotNodeData()

idsm.all_nodes.DtoH()
res = 101
xres = np.linspace(0,1,res,dtype=np.float32)
yres = np.linspace(0,1,res,dtype=np.float32)
mesh = np.meshgrid(xres,yres)
delta = pl.apply_along_axis(idsm.sample_dSMS_dt,0,mesh)

lx,hx = smds[0].limits
ly,hy = smds[1].limits

x_i = 0
y_i = 1
#speed = 0.01+np.sqrt(delta[x_i]**2 + delta[y_i]**2)
speed = 0.01+0.5*np.sqrt(delta[x_i]**2 + delta[y_i]**2)
# pl.streamplot(mesh[x_i]*(hx-lx)+lx,
#            mesh[y_i]*(hy-ly)+ly,
#            delta[x_i]*(hx-lx)+lx,
#            0.0*delta[y_i]*(hy-ly)+ly,
#            density=1.0,linewidth=speed*0.1,
#            color='k')
# pl.xlim(lx,hx)
# pl.ylim(ly,hy)

class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))

print(delta[x_i].max())
# pl.imshow(delta[x_i],origin='lower',extent=[0,1,0,1],cmap='RdGy')
pl.pcolormesh(mesh[x_i],mesh[y_i],delta[x_i],norm=MidpointNormalize(midpoint=0.),cmap='RdGy')
pl.colorbar()
pl.contour(mesh[x_i],mesh[y_i],delta[x_i],[0])

# skip
s = 4
pl.quiver(mesh[x_i][::s,::s],mesh[y_i][::s,::s],
          delta[x_i][::s,::s],delta[y_i][::s,::s],pivot='mid',scale=250.001)


pl.xlabel(smds[0].name)
pl.ylabel(smds[1].name)
pl.title('dM/dt')
pl.savefig('image.png')
#pl.show()
