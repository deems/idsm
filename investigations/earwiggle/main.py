
from kivy.config import Config
Config.set('kivy','log_level','info')
# Config.set('kivy','log_enable', '0')
Config.write()
from kivy.clock import Clock

from rvit.core import init_rvit,rvit_reconnect

import numpy as np

from idsm.utils import logger
from idsm.node_data import Node
from idsm import SensorimotorDimension, IDSM

from robot import Robot
import pylab as pl

import skivy
import pickle as pickle
import time
current_time = lambda: int(round(time.time() * 1000))

class Model(object):
    def __init__(self,*args,**kwargs) :
        self.frames_per_draw = 1.0
        self.smds = [SensorimotorDimension('m',(-1.0,1.0)),
                     SensorimotorDimension('s',(0.0,1.0)),
                     SensorimotorDimension('i',(0.0,1.0))] ## imagination
        self.DT = 0.01
        self.idsm = IDSM(self.smds,DT=self.DT,
                         parameter_overrides={'N_NODES':2**(12-2),
                                              'k_d':200.0*1.0, # bigger is higher IDSM-resolution (more nodes per unit of SM space)
                                              'k_omega':1.0,
                                              'k_weight_degradation' : 5.0,#5.0*2.0,
                                              'init_random_scale' : 2.0,
                                              'd_random_scale' : 10.0,
                                              'changes_in_random_direction_per_t' : 2.0,
                                              })

        self.robot = Robot(DT=self.DT)
        self.d_light = 0.0
        self.d_light2 = -0.15
        self.init()
        self.reset()
        self.experiment_running = False

        super(Model, self).__init__(**kwargs)

        def iterate(arg) :
            self.iterate()
        Clock.schedule_interval(iterate, 1.0/60.0)

    def init(self) :
        self.reset_time = current_time()
        self.light_is_oscillating = False
        self.light_acceleration = 0.0

        ## nodes as vectors for visualization
        self.nav_sm       = np.zeros((self.idsm.N_NODES*2,2))
        self.nav_si       = np.zeros((self.idsm.N_NODES*2,2))
        self.nav_mi       = np.zeros((self.idsm.N_NODES*2,2))


        self.alpha_for_node_vectors = np.zeros((self.idsm.N_NODES*2,1))
        self.reset()

    def reset(self,seed=5) :
        print('---------------------')
        if seed == 'random' :
            seed = np.random.randint(100000)
        np.random.seed(seed)
        self.robot.reset()
        #self.robot.randomizeState()
        self.experiment_running = 0
        self.light_stop()
        self.robot.iterate()
        self.robot.it = 0
        self.robot.light_pos[:] = 0.001
        self.idsm.reset()
        self.idsm.iteration = 0
        self.it = 0
        self.t = 0.0
        self.idsm.SMS_H[:] = np.array([0.25,self.robot.sensor_state,0.5],dtype=np.float32) ## hohee
        print(self.idsm.SMS_H)

    def light_stop(self) :
        self.light_is_oscillating = False
        self.light_acceleration = 0.0
        self.d_light = 0.0

    def light_move(self,vel=0.2) :
        self.light_is_oscillating = False
        self.light_acceleration = 0.0
        self.d_light = vel

    def light_oscillate(self) :
        self.light_is_oscillating = True

    def light_accelerate(self) :
        self.light_acceleration = 0.05

    def relocate_robot(self) :
        d = self.robot.light_pos - self.robot.x 
        self.robot.x[:] = self.robot.light_pos + d
        
    def iterate(self) :
        ## simulate x times
        for _ in range(int(self.frames_per_draw)) :
            if self.experiment_running == 1 :
                self.experiment1_callback()
            elif self.experiment_running == 2:
                self.experiment2_callback()
            elif self.experiment_running == 3:
                self.experiment3_callback()

            # time_running = (current_time() - self.reset_time)/1000.0

            self.it+=1
            self.t = self.DT*self.it
            self.idsm.iterate()
            self.idsm.all_nodes.DtoH()

            if self.experiment_running == 3 :
                time = float(self.it) * self.DT
                if self.it < int(self.training_duration/self.DT) :
                    #self.robot.m[:] = 0.0*np.cos(time*2.0)
                    self.idsm.SMS_H[0] = 0.5+0.1*np.sin(time*2.0)

            # update motor for robot
            #self.robot.m[:] = 2.0*(self.idsm.SMS_H[0]-0.5)
            self.robot.m[:] = np.sin(self.idsm.SMS_H[0]*2.0*np.pi) ## PERIODIC SM SPAC


            if self.light_is_oscillating :
                self.d_light = 0.2*np.sin(self.t*1.0)
                #self.d_light = 0.3*np.sin(self.t*1.0)

            self.d_light += self.light_acceleration*self.DT
                
            self.robot.light_pos[:] += self.DT*self.d_light
            if self.robot.light_pos[:] > 1.0 :
                self.robot.light_pos[:] += -2.0
            if self.robot.light_pos[:] < -1.0 :
                self.robot.light_pos[:] += 2.0

            if self.robot.light2_power > 0 :
                self.robot.light2_pos[:] += self.DT*self.d_light2
                if self.robot.light2_pos[:] > 1.0 :
                    self.robot.light2_pos[:] += -2.0
                if self.robot.light2_pos[:] < -1.0 :
                    self.robot.light2_pos[:] += 2.0

            self.robot.iterate()

            # ## update sensor state for IDSM
            self.idsm.SMS_H[0] = (self.idsm.SMS_H[0] + 1.0) % 1.0 ## MOTOR
            self.idsm.SMS_H[1] = self.robot.sensor_state ## SENSOR
            self.idsm.SMS_H[2] = np.sin(1.0*self.t)*0.5 + 0.5 ## IMAGINATION

        # print(np.shape(self.idsm.SMS_H[0::2]))
        # quit()
        self.prepare_visualization_data()

    def start_experiment(self,experiment_id) :
        logger.info('COMMENCING Experiment')
        self.reset()
        self.experiment_running = experiment_id
        if experiment_id == 1 :
            self.robot.light2_power = 0.0
            self.robot.light2_pos[:] = -5.0
            self.robot.light1_power = 1.0
        elif experiment_id == 2 :
            self.robot.light1_power = 0.5
            self.robot.light2_power = 0.5
            self.robot.light2_pos[:] = 0.0
        if experiment_id == 3 :
            self.robot.light2_power = 0.0
            self.robot.light2_pos[:] = -5.0
            self.robot.light1_power = 1.0
            self.robot.x[:] = -0.5
        self.start_tracking_state()

    def experiment1_callback(self) :
        self.robot.light2_pos[:] = 100.0
        if self.experiment_running :
            self.track_data()

            ## oscillate 15
            ## stop 5
            ## move 10
            for time,inst in [ (0.0, self.light_stop),
                               (10.0, self.light_move),
                               (20.0, self.light_oscillate),
                               (35.0, self.light_stop),
                               (40.0, lambda : self.light_move(vel=-0.2)),
                               (50.0, self.light_oscillate),
                               (65.0, self.light_stop),
                               (70.0, self.relocate_robot),

                               (75.0, self.light_move),
                               (87.5, self.light_oscillate),
                               (105.0, self.light_stop),
                               (110.0, lambda : self.light_move(vel=-0.2)),
                               (120.0, self.light_oscillate),
                               (135.0, self.light_stop),
                               (140.0, self.relocate_robot),

                               (145.0, self.light_move),
                               (157.5, self.light_oscillate),
                               (175.0, self.light_stop),
                               (180.0, lambda : self.light_move(vel=-0.2)),
                               (190.0, self.light_oscillate),
                               (205.0, self.light_stop),

                               (210.0, self.stop_experiment),
                               # (140.0, self.light_move),
                               # (160.0, self.light_oscillate),
                               # (190.0, self.stop_experiment),
            ] :
                if self.it == int((time) / self.DT) :
                    print(inst)
                    inst()
            # for time,inst in [ (0.0, self.light_stop),
            #                    (10.0, self.light_move),
            #                    (30.0, self.light_oscillate),
            #                    (70.0, self.light_stop),
            #                    (80.0, lambda : self.light_move(vel=-0.2)),
            #                    (100.0, self.light_oscillate),
            #                    (130.0, self.light_stop),
            #                    (135.0, self.relocate_robot),
            #                    (140.0, self.relocate_robot),
            #                    (145.0, self.relocate_robot),
            #                    (150.0, lambda : self.light_move(vel=-0.2)),
            #                    (155.0, self.relocate_robot),
            #                    (160.0, self.relocate_robot),
            #                    (165.0, self.light_oscillate),                               
            #                    (175.0, self.stop_experiment),
            #                    # (140.0, self.light_move),
            #                    # (160.0, self.light_oscillate),
            #                    # (190.0, self.stop_experiment),
            # ] :
            #     if self.it == int((time) / self.DT) :
            #         print(inst)
            #         inst()

    def experiment2_callback(self) :
        if self.experiment_running :
            self.track_data()
            for time,inst in [ (0.0, self.light_move),
                               (250.0, self.stop_experiment),
            ] :
                if self.it == int(time / self.DT) :
                    inst()

    def experiment3_callback(self) :
        self.robot.light2_pos[:] = 100.0
        self.training_duration = 20.0

        if self.experiment_running :
            self.track_data()

            for time,inst in [ (0.0, self.light_stop),
                               # (10.0, self.light_move),
                               # (30.0, self.light_oscillate),
                               # (70.0, self.light_stop),
                               # (80.0, lambda : self.light_move(vel=-0.2)),
                               # (100.0, self.light_oscillate),
                               # (130.0, self.light_stop),
                               # (140.0, self.light_move),
                               # (160.0, self.light_oscillate),
                               # (200.0, self.light_stop),
                               # (210.0, lambda : self.light_move(vel=-0.2)),
                               # (230.0, self.light_oscillate),
                               (270.0, self.stop_experiment),
            ] :
                if self.it == int((time+self.training_duration) / self.DT) :
                    inst()


    def stop_experiment(self) :
        logger.info('Experiment %d COMPLETED' %(self.experiment_running))
        self.experiment_running = 0
        self.stop_tracking_state()

    def start_tracking_state(self) :
        self.state_history = {
            'node_data' : [],
            'r_pos' : [],
            'l_pos' : [],
            'l2_pos' : [],
            'time'  : [],
            'SMS' : [],
            'dSMS' : [],
            'light_status' : [],
            'N_NODES' : self.idsm.N_NODES,
            'N_DIM' : self.idsm.N_DIM,
        }

    def track_data(self) :
        nd = self.idsm.getStateOfAllNodes()
        if (self.it % 1000) == 0 :
            self.state_history['node_data'].append(np.array(nd.H))
        self.state_history['r_pos'].append(self.robot.x[0])
        self.state_history['l_pos'].append(self.robot.light_pos[0])
        self.state_history['l2_pos'].append(self.robot.light2_pos[0])
        self.state_history['time'].append(self.it*self.DT)
        self.state_history['SMS'].append(np.array(self.idsm.SMS_H))
        self.state_history['dSMS'].append(np.array(self.idsm.dSMS))
        if self.light_is_oscillating :
            self.state_history['light_status'].append(10000.0)
        # elif self.light_accelerate != 0 :
        #     self.state_history['light_status'].append(20000.0)
        else :
            self.state_history['light_status'].append(self.d_light)

    def stop_tracking_state(self) :
        pickle.dump(self.state_history, open('experiments/experiment.p','wb') )

    def prepare_visualization_data(self) :
        scale = 0.005
        # ## prepare visualization data    
        self.nav_sm[::2,:]   = self.idsm.all_nodes.locs.T[:,:2]
        self.nav_sm[1::2,:]  = (self.idsm.all_nodes.locs.T[:,:2]
                                + self.idsm.all_nodes.vels.T[:,:2]*scale)

        self.nav_si[::2,:]   = self.idsm.all_nodes.locs.T[:,[1,2]]
        self.nav_si[1::2,:]  = (self.idsm.all_nodes.locs.T[:,[1,2]]
                                + self.idsm.all_nodes.vels.T[:,[1,2]]*scale)

        self.nav_mi[::2,:]   = self.idsm.all_nodes.locs.T[:,[0,2]]
        self.nav_mi[1::2,:]  = (self.idsm.all_nodes.locs.T[:,[0,2]]
                                + self.idsm.all_nodes.vels.T[:,[0,2]]*scale)

        omegas = self.idsm.all_nodes.weights #/ 1000.0
        self.alpha_for_node_vectors[:,0] = np.vstack([omegas,omegas]).T.ravel()



# class SkivyApplication(App):
#     def build(self):
#         return Builder.load_file('sk.kv')

#     def on_stop(self) :
#         skivy.disactivate()

if __name__ == '__main__':
    # if len(sys.argv)>1 :
    #     m = Model(run_experiment=int(sys.argv[1]))
    #     r = 1
    #     init_rvit(m,rvit_file='rvit.kv',window_size=(1200/r,800/r))
    # else :
    m = Model()
    r = 1
    init_rvit(m,rvit_file='rvit.kv',window_size=(1200/r,800/r))
    # m.event_logger.close()

        

# if __name__ == '__main__':
#     skivy.activate()
#     SkivyApplication().run()


#WHAT IF I JUST VARY THE ATTRACTIVE ELEMENT OF THE NODES AS AN EXPERIMENTAL PARAMETER AND LEAVE IT AT THAT?

# I COULD HAVE THE NODES BECOME MORE ATTRACTIVE BY REDUCING THE TOTAL
# ATTRACTIVE COEFF. BY THE AMOUNT OF UNHAPPY/UNVISITED NODES.
