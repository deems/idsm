from pylab import *

def circular_distance(a,b) :
    """this assumes the arena is between -1 and 1"""
    a = (a + 1.0) / 2.0
    b = (b + 1.0) / 2.0

    return min(abs(a-b),abs(1.0-abs(a-b)))


class Robot(object) :
    def __init__(self,DT) :
        self.it = 0
        self.x = np.array([0.0])
        self.m = np.array([0.0])
        self.light_pos = np.array([0.0])
        self.light2_pos = np.array([-5.0])


        self.reset()

    def reset(self) :
        self.x[:] = 0.0
        self.m[:] = 0.0
        self.light_pos[:] = 0.0
        self.light2_pos[:] = -5.0
        self.light1_power = 1.0
        self.light2_power = 0.0
        self.sensor_state = 0.0

        self.t = 0.0
        self.DT = 0.01

        self.history_length = 1000        
        self.x_history = np.zeros(self.history_length)
        self.sensor_history = np.zeros(self.history_length)
        
        
    def randomizeState(self) :
        self.x[:] = np.random.rand()*2.0-1.0
        self.m[:] = np.random.rand()
        
    def iterate(self) :
        self.x_history[self.it%self.history_length] = self.x
        self.sensor_history[self.it%self.history_length] = self.sensor_state
        self.it += 1
        self.t += self.DT

        self.dx = self.m * 0.5
        self.x += self.DT * self.dx

        if self.x > 1.0 :
            self.x -= 2.0
        if self.x < -1.0 :
            self.x += 2.0

        oscillation_period = 5.0
        #self.light_pos = 0.002#*sin(self.t*2.0*pi/oscillation_period)
        #self.sensor_state = 1.0/(1.0+(5.0*(self.x-self.light_pos))**2)

        light1_infl = self.light1_power/(1.0+(5.0*(circular_distance(self.x,self.light_pos)))**2)
        light2_infl = self.light2_power/(1.0+(5.0*(circular_distance(self.x,self.light2_pos)))**2)
        self.sensor_state = light1_infl + light2_infl
