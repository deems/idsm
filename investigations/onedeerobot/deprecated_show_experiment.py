# idsm = IDSM(smds,DT=0.01,
#             parameter_overrides={'N_NODES':2**12,
#                                  'k_d':5000.0})
# idsm.all_nodes.initializeNodeData()
# idsm.set_node_data(experiment_data['node_data'][-1])
# print(min(idsm.all_nodes.weights))
# print(max(idsm.all_nodes.weights))
# #idsm.streamplot()
# # idsm.plotNodeData()
# # show()


def plotStream(iteration) :
    figure(figsize=(5.1,4.25))
    idsm = IDSM(smds,DT=0.01,
                parameter_overrides={'N_NODES':2**12,
                                     'k_d':10000.0})
    idsm.all_nodes.initializeNodeData()
    print(float(iteration)/len(node_data))
    idsm.set_node_data(experiment_data['node_data'][iteration])
    idsm.streamplot()
    # idsm.plotNodeData()

# ## animate streamplot
# for it in [-1] : #xrange(len(node_data)) :     
#     plotStream(it)
#     fixplot()
#     savefig(os.path.join(directory,'stream_animation',title+'_stream_%05d.png'%(it)),dpi=300)
    

def plotTimeSeries() :
    figure()
    subplot2grid((1,1),(0,0))
    #plot(time,r_pos_masked,'k',lw=1,label='Robot')
    plot(time,l2_pos_masked,'0.7',lw=1,label='Light2')
    plot(time,l_pos_masked,'r',lw=1,label='Light')
    ylabel('Robot')
    ylim(-1,1)
    xlim(0,max(time))
    legend()

    # series of points into line segments
    x = time
    y = r_pos_masked
    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)

    
    lc = LineCollection(segments, cmap=cmap, norm=norm,lw=0.5)
    # if colorby == 'light_movement' :
    #     lc.set_array(section_c)
    # else :
    #     print(linspace(seg_start_i,post_i,len(y)))
    #lc.set_array(linspace(0,x,len(x)))
    plt.gca().add_collection(lc)


    # figure()
    # subplot2grid((1,1),(0,0))
    # diff = r_pos-l2_pos
    # diff = np.where(diff<=0.0,diff+2,diff)
    # diff_masked = np.ma.masked_where(abs(np.gradient(diff)) > 0.01, diff)
    # plot(time,diff_masked,'k',lw=1)
    # ylabel('Difference')

    # plot(np.array(y['SMS']),',')#,y['SMS'][1,:])
    # plot(time,y['l_pos'])

    # show()
