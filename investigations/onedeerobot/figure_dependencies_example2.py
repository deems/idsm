from matplotlib import rcParams
rcParams['font.family'] = 'Abyssinica SIL'#'sans-serif'
from pylab import *
import sys,os
import pickle as pickle
from idsm import SensorimotorDimension,IDSM
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm
import matplotlib.patches as mpatches
import matplotlib.gridspec as gridspec


linewidth = 1.0
#############################################################################
### PREPARE DATA
smds = [SensorimotorDimension('m',(-1.0,1.0)),
        SensorimotorDimension('s',(0.0,1.0))]

directory = 'experiments'
title = 'experiment'
#title = 'dependencies_example'

# title = 'habits_can_be_robust'

#experiment_data = pickle.load( open( "experiment.p", "rb" ) )
experiment_data = pickle.load( open( os.path.join(directory,title+'.p'), "rb" ) )
y = experiment_data
N_NODES = experiment_data['N_NODES']
N_DIM   = experiment_data['N_DIM']

for key in experiment_data.keys() :
    exec('%s = array(experiment_data["%s"])' %(key,key))
    print(key)

for d in [0,1] :
    SMS[:,d] *= smds[d].limits[1]-smds[d].limits[0]
    SMS[:,d] += smds[d].limits[0]

    
l_pos_masked = np.ma.masked_where(abs(np.gradient(l_pos)) > 0.01, l_pos)
l2_pos_masked = np.ma.masked_where(abs(np.gradient(l2_pos)) > 0.01, l2_pos)
r_pos_masked = np.ma.masked_where(abs(np.gradient(r_pos)) > 0.01, r_pos)

cmap = 'nipy_spectral' #'brg'

alpha = 0.2
light_states = [
    ('stationary' , ## black
    ListedColormap([(0.0,0.0,0.0,0.0),
                    (0.0,0.0,0.0,alpha),
                    (0.0,0.0,0.0,0.0),
                    (1.0,0.0,0.0,0.0)])),
    ('oscillating' , ## red
    ListedColormap([(0.0,0.0,0.0,0.0),
                    (0.0,0.0,0.0,0.0),
                    (0.0,0.0,0.0,0.0),
                    (1.0,0.0,0.0,alpha*0.5)])),
    ('down' , ## green
    ListedColormap([(0.0,0.4,0.1,alpha),
                    (0.0,0.0,0.0,0.0),
                    (0.0,0.0,0.0,0.0),
                    (0.0,0.0,0.0,0.0)])),
    ('up' , ## blue
    ListedColormap([(0.0,0.0,0.0,0.0),
                    (0.0,0.0,0.0,0.0),
                    (0.0,0.0,1.0,alpha+0.2),
                    (0.0,0.0,0.0,0.0)])),
    ('all' , ## all
    ListedColormap([(0.0,0.6,0.1,alpha),
                    (0.0,0.0,0.0,alpha),
                    (0.0,0.0,1.0,alpha+0.2),
                    (1.0,0.0,0.0,alpha*0.5)]))
]

lightstate_cmap = ListedColormap(['g', 'k', 'b','r'])


## unroll IDSM's SM-space to show actual motor values
#SMS[:,0] = sin(2.0*np.pi*SMS[:,0])


#############################################################################

def plot2DSMS(colorby='light_movement',cmap='nipy_spectral',start_iteration=0) :
    from scipy.spatial.distance import euclidean
    ## trimmed SMS history
    t_SMS = SMS[start_iteration:,:]
    d_SMS = np.array( [euclidean(t_SMS[i,:],t_SMS[i+1]) for i in xrange(shape(t_SMS)[0]-1)] )
    cuts = [x[0] for x in np.argwhere(d_SMS > 0.5)]
    seg_start_i = 0
    cuts.append(len(d_SMS)-1)

    for cut_i in cuts :
        pre_i = cut_i
        post_i = cut_i + 1
        section = t_SMS[seg_start_i:pre_i,:]
        section_color = light_status[seg_start_i:pre_i]

        if(shape(section)[0] > 0) :
            interpolated_start_point = 0.5*(t_SMS[seg_start_i-1,:] +
                                            t_SMS[seg_start_i,:])
            d_cut = t_SMS[seg_start_i,:] - t_SMS[seg_start_i-1,:]
            if d_cut[0] < -0.5 : # right to left
                interpolated_start_point[0] = smds[0].limits[0]
            elif d_cut[0] > 0.5 : # left to right
                interpolated_start_point[0] = smds[0].limits[1]

            interpolated_end_point = 0.5*(t_SMS[pre_i,:] +
                                          t_SMS[post_i,:])
            d_cut = t_SMS[post_i,:] - t_SMS[pre_i,:]
            if d_cut[0] < -0.5 : # right to left
                interpolated_end_point[0] = smds[0].limits[1]
            elif d_cut[0] > 0.5 : # left to right
                interpolated_end_point[0] = smds[0].limits[0]

            section = np.vstack([interpolated_start_point,
                                 section,
                                 interpolated_end_point])


            section_c = np.hstack([section_color[0],section_color,section_color[-1]])

            # series of points into line segments
            x = section[1:,0]
            y = section[1:,1]
            points = np.array([x, y]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)

            if True :
                ### FAST PLOT
                if colorby == 'light_movement' :
                    #cmap = lightstate_cmap
                    norm = BoundaryNorm([-1, -0.1, 0.1, 1.0, 10000.0], cmap.N)
                else :
                    norm = plt.Normalize(0,len(time))
                    alpha = 0.5

                lc = LineCollection(segments, cmap=cmap, norm=norm,lw=1.0)
                if colorby == 'light_movement' :
                    lc.set_array(section_c)
                else :
                    lc.set_array(linspace(seg_start_i,post_i,len(y)))
                plt.gca().add_collection(lc)
            else :
                ### GOOD ALPHA PLOT
                light_status_to_color = {
                    -0.2  : 'g',
                    0.0   : 'k',
                    0.2   : 'b',
                    10000 : 'r',
                }
                section_c = [light_status_to_color[x] for x in section_c]
                for seg_i in xrange(shape(segments)[0]) :
                    plot(segments[seg_i][:,0],
                         segments[seg_i][:,1],
                         alpha=0.1,lw=1,color=section_c[seg_i],solid_capstyle="butt")
                ### END GOOD ALPHA PLOT
        seg_start_i = post_i
    xlim(smds[0].limits)
    ylim(0,1.05)
    # if colorby == 'light_movement' :
    #     red_patch = mpatches.Patch(color='r', label='oscillating')
    #     blue_patch = mpatches.Patch(color='b', label='right')
    #     green_patch = mpatches.Patch(color='g', label='left')
    #     black_patch = mpatches.Patch(color='k', label='stationary')
    #     plt.legend(handles=[red_patch,blue_patch,green_patch,black_patch],
    #                bbox_to_anchor=(-0.1, 1.11), loc=2, borderaxespad=0.)

    # show()


def plotTimeSeries(startt,stopt,colorby='light_movement') :
    global cmap
    cuts = [x[0] for x in np.argwhere(abs(np.gradient(r_pos)) > 0.01)]
    seg_start_i = 0
    cuts.append(len(r_pos)-2)

    for cut_i in cuts :
        pre_i = cut_i
        post_i = cut_i + 1
        section = r_pos[seg_start_i:pre_i]
        section_color = light_status[seg_start_i:pre_i]
        
        if(shape(section)[0] > 0) :
            interpolated_start_point = 0.5*(r_pos[seg_start_i-1] +
                                            r_pos[seg_start_i])
            d_cut = r_pos[seg_start_i] - r_pos[seg_start_i-1]
            if d_cut < -0.5 : # right to left
                interpolated_start_point = 1.0
            elif d_cut > 0.5 : # left to right
                interpolated_start_point = -1.0

            interpolated_end_point = 0.5*(r_pos[pre_i] +
                                          r_pos[post_i])
            d_cut = r_pos[post_i] - r_pos[pre_i]
            if d_cut < -0.5 : # right to left
                interpolated_end_point = 1.0
            elif d_cut > 0.5 : # left to right
                interpolated_end_point = -1.0

            section = [interpolated_start_point]+list(section)+[interpolated_end_point]
            section_c = np.hstack([section_color[0],section_color,section_color[-1]])

            # series of points into line segments
            x = time[seg_start_i:post_i+1]
            y = section[:]

            points = np.array([x, y]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)

            if colorby == 'light_movement' :
                cmap = lightstate_cmap
                norm = BoundaryNorm([-1, -0.1, 0.1, 1.0, 10000.0], cmap.N)
                alpha = 0.4
            else :
                norm = plt.Normalize(0,len(time))
                alpha = 0.5

            lc = LineCollection(segments, cmap=cmap, norm=norm,alpha=1.0,lw=linewidth)
            if colorby == 'light_movement' :
                lc.set_array(section_c)
            else :
                lc.set_array(linspace(seg_start_i,post_i,len(y)))
            plt.gca().add_collection(lc)
        seg_start_i = post_i
    plot(time,l_pos_masked,'0.5',ls='--',lw=linewidth,label='Light')
    #plot(time,l2_pos_masked,'0.4',lw=1,label='Light2')

    xlim(startt,stopt)
    ylim(-1.0,1.0)
    # if colorby == 'light_movement' :
    #     red_patch = mpatches.Patch(color='r', label='oscillating')
    #     blue_patch = mpatches.Patch(color='b', label='right')
    #     green_patch = mpatches.Patch(color='g', label='left')
    #     black_patch = mpatches.Patch(color='k', label='stationary')
    #     plt.legend(handles=[red_patch,blue_patch,green_patch,black_patch],
    #                bbox_to_anchor=(-0.1, 1.11), loc=2, borderaxespad=0.)

    # show()

def fixplot() :
    lowx,highx = -1.0,+1.0
    lowy,highy = 0.6,+1.
    # lowx,highx = -1.0,+1.0
    # lowy,highy = 0.825,+.9
    xlim(lowx,highx)
    ylim(lowy,highy)
    #  xticks = #[-0.8,-0.4,0.0,0.4,0.8]
    xticks = [-1.0,-0.5,0.0,0.5,1.0]
    gca().set_xticks(xticks)
    gca().set_xticklabels(['%0.2f'%sin(2.0*np.pi*(x+1.0)/2.0) for x in xticks])
    yticks = [0.65,0.75,0.85,0.95]
    gca().set_yticks(yticks)
    ax.set_xlabel('x')
    #xlabel(smds[0].name)
    ylabel(smds[1].name)


def plotStream() :
    idsm = IDSM(smds,DT=0.01,
                parameter_overrides={'N_NODES':2**10,
                                     'k_d':10000.0/50})
    idsm.all_nodes.initializeNodeData()
    idsm.set_node_data(node_data[-2])
    # print(len(node_data))
    #idsm.streamplot()
    #idsm.plotNodeData()

    xdim_i,ydim_i = 0,1
    nodedata = idsm.all_nodes
    colors = [x/2.0 for x in idsm.omegas()]
    quiver(2.0*nodedata.locs[xdim_i,:]-1.0,nodedata.locs[ydim_i,:],
           2.0*nodedata.vels[xdim_i,:]-1.0,nodedata.vels[ydim_i,:],
           colors,alpha=0.5,
           pivot='middle',width=0.01,cmap='binary',
           scale=33.0,
    )

    
## RUN
#colorby = 'age'
colorby = 'light_movement'

figscale = 1.1
figure(figsize=(figscale*4.75,figscale*6.8))
gs = gridspec.GridSpec(4, 2,
                       width_ratios=[1,1],
                       height_ratios=[1,1,1,0.2]
                       )
for subfig_i,(name,cmap) in enumerate(light_states) :
    ax = plt.subplot(gs[subfig_i])
    #ax.set_title(name)
    text(0.03,0.9,name,transform=ax.transAxes)
    ### DO NOT TRUST THIS START IT CRAP
    # if False : # name == 'all' :
    #     start_it = 0
    # else :
    #     start_it = 70*100
    start_it = 0 
    plot2DSMS(colorby=colorby,cmap=cmap,start_iteration=start_it)
    fixplot()
    if subfig_i in [0,1,2,3,4,5] :
        ax.set_xticklabels( [] )
        ax.set_xlabel('')
    if subfig_i in [1,3,5] :
        ax.set_yticklabels( [] )
        ax.set_ylabel('')
    #break
              
ax = plt.subplot(gs[5])
plotStream()
fixplot()
ylim(0.6,1.0)
ax.set_xticklabels( [] )
ax.set_yticklabels( [] )
xlabel('')
ylabel('')
text(0.03,0.9,'nodes',transform=ax.transAxes)

ax=plt.subplot(gs[6])
x=linspace(0,1,501)
gca().set_xticks([])#[0.,0.25,0.5,0.75,1])
gca().set_yticks([-1,0,1])
plot(x,sin(2.0*np.pi*x))
xlabel('x')
ylabel('m')
gca().spines['left'].set_position('zero')
gca().spines['right'].set_color('none')
gca().spines['bottom'].set_position('zero')
gca().spines['top'].set_color('none')
gca().xaxis.set_ticks_position('bottom')

ax=plt.subplot(gs[7])
gca().set_xticks([])#[0.,0.25,0.5,0.75,1])
gca().set_yticks([])
plot(x,sin(2.0*np.pi*x))
xlabel('x')
gca().spines['left'].set_position('zero')
gca().spines['right'].set_color('none')
gca().spines['bottom'].set_position('zero')
gca().spines['top'].set_color('none')
gca().xaxis.set_ticks_position('bottom')


tight_layout()    
savefig(os.path.join(directory,title+'_trajectories.png'),dpi=300)

t0 = 0.0
t1 = 70.0
t2 = 140.0
tk = 2.5

lightstate_cmap = light_states[-1][-1]
figscale = 0.7
figure(figsize=(figscale*8.25*2.0,figscale*6.75))
gs = gridspec.GridSpec(3, 1,
                       width_ratios=[1],
                       height_ratios=[1,1,1]
                       )
ax = plt.subplot(gs[0])
#text(0.97,0.75,'A',transform=ax.transAxes,fontsize=18)
# plotTimeSeries(0,max(time),colorby=colorby)
plotTimeSeries(0,t1+tk,colorby=colorby)
ylabel('pos.')


ax = plt.subplot(gs[1])
#text(0.97,0.75,'B',transform=ax.transAxes,fontsize=18)
# plotTimeSeries(95,135,colorby=colorby)
plotTimeSeries(t1-tk,t2+tk,colorby=colorby)
ylabel('pos.')

ax = plt.subplot(gs[2])
#text(0.97,0.75,'C',transform=ax.transAxes,fontsize=18)
# plotTimeSeries(95,135,colorby=colorby)
plotTimeSeries(t2-tk,max(time),colorby=colorby)
ylabel('pos.')


xlabel('time')
tight_layout()
savefig(os.path.join(directory,title+'_timeseries.png'),dpi=300)
#show()
