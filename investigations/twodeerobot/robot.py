from pylab import *
from scipy.spatial.distance import euclidean

def circular_distance(a,b) :
    """this assumes the arena is between -1 and 1"""
    a = (a + 1.0) / 2.0
    b = (b + 1.0) / 2.0

    return min(abs(a-b),abs(1.0-abs(a-b)))

class Light(object) :
    def __init__(self) :
        self.x = np.array([0.5])
        self.y = np.array([0.5])        

        self.reset()

    def reset(self) :
        self.x[:] = 0.5
        self.y[:] = 0.5

    def impact_sensor(self,sensor_x,sensor_y,sensor_angle) :
        accum = 0.0
        ## compensating for wrap around light viewing
        ## acting as if there are lights at all of these offsets
        for lox,loy in [[-1.,0.],
                        [+1.,0.],
                        [0.,-1.],
                        [0.,+1.],
                        [0.,0.]] :
            lx = self.x + lox
            ly = self.y + loy
            
            dSq = (sensor_x - lx)**2 + (sensor_y - ly)**2

            # if the sensor were omnidirectional, its value would be
            falloff = 0.025*10.0 # lower number, sensors fall off faster
            omni = falloff/(falloff+dSq)

            # ## ... but instead, we are going to have a linear falloff
            # omni = max(0.0,1.0-dSq)
                        
            # calculate attenuation due to directionality
            # sensor to light unit vector
            s2l = [lx - sensor_x,
                   ly - sensor_y]
            s2l_mag = np.sqrt(s2l[0]**2 + s2l[1]**2)
            if s2l_mag > 0.0 :
                s2l = [v / s2l_mag for v in s2l]

            # sensor direction unit vector
            sd = [cos(sensor_angle),
                  sin(sensor_angle)]

            # positive set of dot product
            attenuation = max(0.0,s2l[0]*sd[0] + s2l[1]*sd[1])

            accum += omni * attenuation
        #     print('omni',omni)
        # quit()
        return accum

class Robot(object) :
    def __init__(self) :
        self.RADIUS = 0.0
        
        self.it = 0
        self.pos = np.array([0.0,0.0,0.0])

        self.history_length = 1000

        self.pos_history = np.random.rand(self.history_length,3)

        self.lm = np.array([0.0])
        self.rm = np.array([0.0])

        self.lights = []

        self.reset()

    def reset(self) :
        self.pos[:] = [0.0,0.0,0.0]

        ## motors
        self.lm[:] = 0.0
        self.rm[:] = 0.0

        ## sensors
        self.ls = 0.0
        self.rs = 0.0

        self.t = 0.0
        self.DT = 0.01

    def randomizeState(self) :
        self.pos[:] = [np.random.rand(),np.random.rand(),np.random.rand()*2.0*np.pi]

    def updateSensors(self) :
        ## calculate sensor positions
        beta = np.pi / 5.0
        x,y,a = self.pos
        lsx = x + cos(a+beta)*self.RADIUS
        lsy = y + sin(a+beta)*self.RADIUS
        lsa = a + beta
        rsx = x + cos(a-beta)*self.RADIUS
        rsy = y + sin(a-beta)*self.RADIUS
        rsa = a - beta

        ## calculate light impacts on sensors
        self.ls = self.rs = 0.0
        for light in self.lights :
            self.ls += light.impact_sensor(lsx,lsy,lsa)
            self.rs += light.impact_sensor(rsx,rsy,rsa)

    def iterate(self) :
        x,y,a = self.pos
        self.pos_history[self.it%self.history_length,:] = x,y,a

        self.updateSensors()

        self.it += 1
        self.t += self.DT

        speed = 0.1
        self.dx = speed * cos(a)*(self.lm+self.rm)
        self.dy = speed * sin(a)*(self.lm+self.rm)
        self.da = speed * 2.0 * (self.rm-self.lm) * 2.0        

        self.pos[0] += self.DT * self.dx
        self.pos[1] += self.DT * self.dy
        self.pos[2] += self.DT * self.da
        
        if self.pos[0] > 1.0 :
            self.pos[0] -= 1.0
        if self.pos[0] < -0.0 :
            self.pos[0] += 1.0
        if self.pos[1] > 1.0 :
            self.pos[1] -= 1.0
        if self.pos[1] < -0.0 :
            self.pos[1] += 1.0


def test_directional_light_sensors() :
    r = Robot()
    l = Light()
    l.x[:] = 0.01
    l.y[:] = 0.5
    r.lights.append(l)

    # l = Light()
    # l.x[:] = 0.5
    # l.y[:] = 0.5
    # r.lights.append(l)

    #r.a[:] = np.pi/4

    res = linspace(0,1,50)
    xs,ys = mesh = meshgrid(res,res)

    def f(coords) :
        r.pos[0] = coords[0]
        r.pos[1] = coords[1]
        r.updateSensors()
        return r.ls[0]#+r.rs[0]

    zs = apply_along_axis(f,0,mesh)
    print(shape(zs))
    imshow(zs)
    show()

#test_directional_light_sensors()
