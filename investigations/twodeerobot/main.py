
from kivy.config import Config
Config.set('kivy','log_level','info')
# Config.set('kivy','log_enable', '0')
Config.write()
from kivy.clock import Clock

from rvit.core import init_rvit,rvit_reconnect

import numpy as np

from idsm.utils import logger
from idsm.node_data import Node
from idsm import SensorimotorDimension, IDSM

from robot import Robot,Light
import pylab as pl

import skivy
import pickle as pickle
import time
current_time = lambda: int(round(time.time() * 1000))

# from kivy import platform
# from kivy.config import Config
# if platform == 'linux' :
#     ratio = 2.0
#     s = 1.5
#     Config.set('graphics', 'width',  str(int(1500/s)))
#     Config.set('graphics', 'height', str(int(1000/s)))
#     #Config.set('graphics', 'fullscreen', 'auto')

# from kivy.app import App
# from kivy.uix.widget import Widget
# from kivy.clock import Clock
# from kivy.lang import Builder

# import numpy as np

# from idsm.utils import logger
# from idsm.node_data import Node
# from idsm import SensorimotorDimension, IDSM


# import skivy
# import cPickle as pickle
# import time
# current_time = lambda: int(round(time.time() * 1000))

class Model(object):
    def __init__(self,*args,**kwargs) :
        self.frames_per_draw = 10.0
        self.smds = [SensorimotorDimension('ls',(0.0,1.0)),
                     SensorimotorDimension('rs',(0.0,1.0)),
                     SensorimotorDimension('lm',(-1.0,1.0)),
                     SensorimotorDimension('rm',(-1.0,1.0)),]
        self.DT = 0.01
        # self.idsm = IDSM(self.smds,DT=self.DT,
        #                  parameter_overrides={'N_NODES':2**(12-1),
        #                                       'k_d':10000.0/50, # bigger is higher IDSM-resolution (more nodes per unit of SM space)
        #                                       'k_weight_degradation' : 5.0*0.2,
        #                                       'k_weight_reinforcement' : 1000.0*0.2,
        #                                       'random_motor_activity_threshold' : 0.5,
        #                                       'changes_in_random_direction_per_t' :  10.0,
        #                                       'd_random_scale' : 5.0,
        #                                       'k_activation_delay' : 1.0*1.0,
        #                  })
        self.idsm = IDSM(self.smds,DT=self.DT,
                         parameter_overrides={'N_NODES':2**(9-1),
                                              'k_d':10000.0/100, # bigger is higher IDSM-resolution (more nodes per unit of SM space)
                                              'k_weight_degradation' : 5.0*0.2,
                                              'k_weight_reinforcement' : 1000.0*0.2,
                                              'random_motor_activity_threshold' : 0.5,
                                              'changes_in_random_direction_per_t' :  10.0,
                                              'd_random_scale' : 5.0,
                                              'k_activation_delay' : 1.0*1.0,
                         })

        self.robot = Robot()
        self.light = Light()
        self.robot.lights.append(self.light)
        self.init()
        self.reset()
        self.experiment_running = False

        self.randomness_history = np.ones(self.robot.history_length)
        
        super(Model, self).__init__(**kwargs)

        def iterate(arg) :
            self.iterate()
        Clock.schedule_interval(iterate, 1.0/60.0)

    def init(self) :
        self.reset_time = current_time()
        self.sensor_dim_nodes_as_vectors       = np.zeros((self.idsm.N_NODES*2,2))
        self.motor_dim_nodes_as_vectors       = np.zeros((self.idsm.N_NODES*2,2))
        self.alpha_for_node_vectors = np.zeros((self.idsm.N_NODES*2,1))
        self.reset()

    def reset(self,seed=5) :
        print('---------------------')
        if seed == 'random' :
            seed = np.random.randint(100000)
        np.random.seed(seed)
        self.robot.reset()
        self.robot.lm[:] = np.random.rand()*2.0 - 1.0
        self.robot.rm[:] = np.random.rand()*2.0 - 1.0

        self.experiment_running = 0
        self.robot.iterate()
        self.robot.it = 0

        self.idsm.reset()
        self.idsm.iteration = 0
        self.it = 0
        self.t = 0.0
        self.idsm.SMS_H[:] = np.array([self.robot.ls,
                                       self.robot.rs,
                                       0.0,0.0],dtype=np.float32)

    def iterate(self) :
        ## simulate x times
        for _ in range(int(self.frames_per_draw)) :
            # if self.experiment_running == 1 :
            #     self.experiment1_callback()
            # elif self.experiment_running == 2:
            #     self.experiment2_callback()
            # elif self.experiment_running == 3:
            #     self.experiment3_callback()

            self.it+=1
            self.t = self.DT*self.it
            time = float(self.it) * self.DT
            self.idsm.iterate()
            self.idsm.all_nodes.DtoH()

            ## training
            # if self.experiment_running == 3 :
            #     time = float(self.it) * self.DT
            #     if self.it < int(self.training_duration/self.DT) :
            #         #self.robot.m[:] = 0.0*np.cos(time*2.0)
            #         self.idsm.SMS_H[0] = 0.5+0.333*np.sin(time*2.0)
            
            # sensorimotor space is made to be periodic in motor dimensions
            # self.robot.lm[:] = np.sin(self.idsm.SMS_H[2]*2.0*np.pi)
            # self.robot.rm[:] = np.sin(self.idsm.SMS_H[3]*2.0*np.pi)

            ## this is a crazy space!
            ## k = 2.0; p =3; plot(x,sin(2*(1.0+x)*k*2.0*np.pi*x**p))
            k = 2.0
            p = 3
            self.robot.lm[:] = np.sin(k*(1.0+self.idsm.SMS_H[2])*2.0*np.pi*self.idsm.SMS_H[2]**p)
            self.robot.rm[:] = np.sin(k*(1.0+self.idsm.SMS_H[3])*2.0*np.pi*self.idsm.SMS_H[3]**p)
            
            
            # # if self.light_is_oscillating :
            # self.light.x[:] = 0.5 + 0.333*np.cos(time*0.1)
            # self.light.y[:] = 0.5 + 0.333*np.sin(time*0.1)
            self.light.x[:] = 0.5 + 0.333*np.cos(0.1*time+np.sin(time*0.1)*1.0)
            self.light.y[:] = 0.5 + 0.333*np.sin(0.1*time+np.sin(time*0.1)*1.0)

            self.robot.iterate()
            self.randomness_history[self.robot.it%self.robot.history_length] = self.idsm.random_amount[0]


            # ## update sensor state for IDSM
            self.idsm.SMS_H[0] = self.robot.ls
            self.idsm.SMS_H[1] = self.robot.rs
            self.idsm.SMS_H[2] = (self.idsm.SMS_H[2] + 1.0) % 1.0
            self.idsm.SMS_H[3] = (self.idsm.SMS_H[3] + 1.0) % 1.0

        self.prepare_visualization_data()

    # def start_experiment(self,experiment_id) :
    #     logger.info('COMMENCING Experiment')
    #     self.reset()
    #     self.experiment_running = experiment_id
    #     if experiment_id == 1 :
    #         self.robot.light2_power = 0.0
    #         self.robot.light2_pos[:] = -5.0
    #         self.robot.light1_power = 1.0
    #     elif experiment_id == 2 :
    #         self.robot.light1_power = 0.5
    #         self.robot.light2_power = 0.5
    #         self.robot.light2_pos[:] = 0.0
    #     if experiment_id == 3 :
    #         self.robot.light2_power = 0.0
    #         self.robot.light2_pos[:] = -5.0
    #         self.robot.light1_power = 1.0
    #         self.robot.x[:] = -0.5
    #     self.start_tracking_state()

    # def experiment1_callback(self) :
    #     self.robot.light2_pos[:] = 100.0
    #     if self.experiment_running :
    #         self.track_data()

    #         for time,inst in [ (0.0, self.light_stop),
    #                            (10.0, self.light_move),
    #                            (30.0, self.light_oscillate),
    #                            (70.0, self.light_stop),
    #                            (80.0, lambda : self.light_move(vel=-0.2)),
    #                            (100.0, self.light_oscillate),
    #                            (130.0, self.light_stop),
    #                            (140.0, self.light_move),
    #                            (160.0, self.light_oscillate),
    #                            (190.0, self.stop_experiment),
    #         ] :
    #             if self.it == int((time) / self.DT) :
    #                 inst()

    # def stop_experiment(self) :
    #     logger.info('Experiment %d COMPLETED' %(self.experiment_running))
    #     self.experiment_running = 0
    #     self.stop_tracking_state()

    def start_tracking_state(self) :
        self.state_history = {
            'node_data' : [],
            'r_pos' : [],
            'l_pos' : [],
            'l2_pos' : [],
            'time'  : [],
            'SMS' : [],
            'dSMS' : [],
            'light_status' : [],
            'N_NODES' : self.idsm.N_NODES,
            'N_DIM' : self.idsm.N_DIM,
        }

    def track_data(self) :
        nd = self.idsm.getStateOfAllNodes()
        # if (self.it % 10) == 0 :
        #     self.state_history['node_data'].append(np.array(nd.H))
        self.state_history['r_pos'].append(self.robot.x[0])
        self.state_history['l_pos'].append(self.robot.light_pos[0])
        self.state_history['l2_pos'].append(self.robot.light2_pos[0])
        self.state_history['time'].append(self.it*self.DT)
        self.state_history['SMS'].append(np.array(self.idsm.SMS_H))
        self.state_history['dSMS'].append(np.array(self.idsm.dSMS))
        # if self.light_is_oscillating :
        #     self.state_history['light_status'].append(10000.0)
        # else :
        #     self.state_history['light_status'].append(self.d_light)

    def stop_tracking_state(self) :
        pickle.dump(self.state_history, open('experiments/experiment.p','wb') )

    def prepare_visualization_data(self) :
        # ## prepare visualization data    
        self.sensor_dim_nodes_as_vectors[::2,:]   = self.idsm.all_nodes.locs.T[:,:2]
        self.sensor_dim_nodes_as_vectors[1::2,:]  = (self.idsm.all_nodes.locs.T[:,:2]
                                                     + self.idsm.all_nodes.vels.T[:,:2]*0.01)

        self.motor_dim_nodes_as_vectors[::2,:]   = self.idsm.all_nodes.locs.T[:,2:]
        self.motor_dim_nodes_as_vectors[1::2,:]  = (self.idsm.all_nodes.locs.T[:,2:] +
                                                    self.idsm.all_nodes.vels.T[:,2:]*0.005)

        # self.sensor_dim_nodes_as_vectors[::2,:]   = self.idsm.all_nodes.locs.T[:,[1,2]]
        # self.sensor_dim_nodes_as_vectors[1::2,:]  = (self.idsm.all_nodes.locs.T[:,[1,2]]
        #                                              + self.idsm.all_nodes.vels.T[:,:2]*0.01)

        # self.motor_dim_nodes_as_vectors[::2,:]   = self.idsm.all_nodes.locs.T[:,2:]
        # self.motor_dim_nodes_as_vectors[1::2,:]  = (self.idsm.all_nodes.locs.T[:,2:] +
        #                                             self.idsm.all_nodes.vels.T[:,2:]*0.01)
                
        omegas = self.idsm.omegas()
        self.alpha_for_node_vectors[:,0] = np.vstack([omegas,omegas]).T.ravel()



# class SkivyApplication(App):
#     def build(self):
#         return Builder.load_file('sk.kv')

#     def on_stop(self) :
#         skivy.disactivate()




if __name__ == '__main__':
    # if len(sys.argv)>1 :
    #     m = Model(run_experiment=int(sys.argv[1]))
    #     r = 1
    #     init_rvit(m,rvit_file='rvit.kv',window_size=(1200/r,800/r))
    # else :
    m = Model()
    r = 1
    init_rvit(m,rvit_file='rvit.kv',window_size=(1200/r,800/r))
    # m.event_logger.close()
