from pylab import *
from matplotlib import cm
from plotting_utils import *

def animation(trial_path) :
    matplotlib.use("Agg")
    trial_name = trial_path.rsplit('/', 1)[-1]
    vis_path = create_visualization_folder(trial_path)

    pickle_obj_path = os.path.join(trial_path,f'pickle_objs.pkl')
    with open(pickle_obj_path, 'rb') as file:
        B = pickle.load(file)

    motor_state_to_motor_value = B['motor_state_to_motor_value']
    
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)
    frequently_times = np.load(trial_path+'/frequently_times.npy',allow_pickle=True)

    r_pos = np.load(trial_path+'/robot_position.npy',allow_pickle=True)
    l_pos = np.load(trial_path+'/light_position.npy',allow_pickle=True)
    rx,ry,ra = r_pos[:,0],r_pos[:,1],r_pos[:,2]
    lx,ly = l_pos[:,0],l_pos[:,1]

    color_data,color_data_f,cmap = segment_data_to_colors(trial_path)
    mapping = cm.get_cmap(cmap)
    #rgb = np.array([mapping(v) for v in color_data/max(color_data)])
    rgb = np.array([mapping(v) for v in color_data/CMAP_VMAX])

    nodes = data_to_nodes(trial_path)
    sms = np.load(trial_path+'/sensorimotor_state.npy',allow_pickle=True)


    color_data,color_data_f,cmap = segment_data_to_colors(trial_path)
    c = color_data_f # f might be wrong here?

    marker_scale = 40.0

    ## setup figures
    fig = figure(figsize=(12,12))
    rows,cols = 2,2

    scpars = {
        's' : 60,
        'cmap' : cmap,
        'alpha': 0.5,
        'vmin' : CMAP_VMIN,
        'vmax' : CMAP_VMAX
        }

    smotors = subplot2grid((rows,cols),(0,0))
    smotors.set_aspect('equal')
    smotors.set_title('motor');
    smotors.set_xlabel('lm-sms');
    smotors.set_ylabel('rm-sms');
    smotors.set_xlim(-0.1,1.1);
    smotors.set_ylim(-0.1,1.1)
    smscatter = smotors.scatter([],[],**scpars)
   
    sensors = subplot2grid((rows,cols),(0,1))
    sensors.set_aspect('equal')
    sensors.set_xlim(-0.1,1.1)
    sensors.set_ylim(-0.1,1.1)
    sensors.set_title('sensor')
    sensors.set_xlabel('ls')
    sensors.set_ylabel('rs')
    sscatter = sensors.scatter([],[],**scpars)

    motors = subplot2grid((rows,cols),(1,1))
    motors.set_aspect('equal')
    motors.set_title('motor');
    motors.set_xlabel('lm');
    motors.set_ylabel('rm');
    motors.set_xlim(-0.1,1.1);
    motors.set_ylim(-0.1,1.1)
    mscatter = motors.scatter([],[],**scpars)
    
    sms_tail = smotors.scatter([],[],s=5,c='k')
    ms_tail = motors.scatter([],[],s=5,c='k')
    ss_tail = sensors.scatter([],[],s=5,c='k')

    ## spatial subplot
    ax = subplot2grid((rows,cols),(1,0))
    ax.set_aspect('equal')
    ax.set_title('');
    ax.set_xlabel('');
    ax.set_ylabel('');
    ax.set_xlim(-0.0,1.0);
    ax.set_ylim(-0.0,1.0)
    r = ax.scatter([],[],s=20)
    light_circle = plt.Circle((0.5, 0.5), 0.05, fc='#ffff00',ec='k')
    ROBOT_RADIUS = 0.04
    robot_circle = plt.Circle((0.5, 0.5), ROBOT_RADIUS, fc='#333333',ec='k')
    robot_line,  = plt.plot([0,0],[0,0],color='0.9',lw=1)
    
    ittext = motors.text(-0.1,-0.11, "", bbox={'facecolor':'k', 'alpha':1.0, 'pad':3},
                         transform=motors.transAxes, ha="center",color='w')

    skip = 25
    #skip = 10000
    tail_length = 5000

    tight_layout()

    
    def init():
        mscatter.set_offsets([])
        ax.add_patch(light_circle)
        ax.add_patch(robot_circle)
        return mscatter,r,light_circle,robot_circle,robot_line

    def update_fig(frame_i) :
        ## update spatial plot
        iteration = frame_i * skip
        print(f'frame: {frame_i}\t iteration:{iteration}')
        a = max(0,iteration-tail_length)
        b = iteration

        r.set_offsets(r_pos[a:b,:2])
        r.set_color(rgb[a:b])

        light_circle.center = (lx[b],ly[b])
        robot_circle.center = (r_pos[b,0],r_pos[b,1])

        xo = r_pos[b,0] + np.cos(r_pos[b,2])*ROBOT_RADIUS
        yo = r_pos[b,1] + np.sin(r_pos[b,2])*ROBOT_RADIUS
        robot_line.set_data([r_pos[b,0],xo],[r_pos[b,1],yo])

        ittext.set_text(f't={sample_times[iteration]}')

        if (iteration % 100) == 0 :
            ## update sm-space plots
            freq_sample_index = iteration//100 #* 100
            print('fsi:',freq_sample_index)

            b = max(0,iteration-tail_length)
            d = iteration
            tail_colors = np.array([np.zeros(d-b),
                                    np.zeros(d-b),
                                    np.zeros(d-b),
                                    linspace(0,1,d-b)]).T

            sms_tail.set_offsets(sms[b:d,2:4])
            ms_tail.set_offsets(motor_state_to_motor_value(sms[b:d,2:4]))
            ss_tail.set_offsets(sms[b:d,:2])

            sms_tail.set_color(tail_colors)
            ms_tail.set_color(tail_colors)
            ss_tail.set_color(tail_colors)

            # print(f'index: {index}\t iteration:{iteration}')

            active_nodes = [node for node in nodes
                            if (node['b_i'] < iteration and node['d_i'] > iteration)]
            locs = np.array([node['loc'] for node in active_nodes])

            if len(active_nodes) > 0:
                sm = locs[:,2:4]
                m = motor_state_to_motor_value( locs[:,2:4] )
                s = locs[:,0:2]
                smscatter.set_offsets(sm)
                mscatter.set_offsets(m)
                sscatter.set_offsets(s)
                ages = [(iteration-node['b_i'])//100 for node in active_nodes]
                weights = [node['weight_h'][age]*marker_scale for node,age in zip(active_nodes,ages)]
                segment_indices = [node['segment_index'] for node in active_nodes]
                N_COLORS_IN_CMAP = 10
                colors = [cmap(segment_index/N_COLORS_IN_CMAP) for segment_index in segment_indices]
                n = active_nodes[-1]            
                #print(f'n[b_i]: {n["b_i"]}\t segment:{segment_indices[-1]:0.2f}\t color:{colors[-1]}')
                smscatter.set_sizes(weights)
                mscatter.set_sizes(weights)
                sscatter.set_sizes(weights)            
                smscatter.set_color(colors)
                mscatter.set_color(colors)
                sscatter.set_color(colors)
            else :
                smscatter.set_offsets(np.zeros((0,2)))
                mscatter.set_offsets(np.zeros((0,2)))
                sscatter.set_offsets(np.zeros((0,2)))

        return smscatter,mscatter,sscatter,ittext,sms_tail,ms_tail,ss_tail,r,light_circle,robot_circle,robot_line

    from matplotlib import animation

    anim = animation.FuncAnimation(fig, update_fig, init_func=init, frames=len(sample_times)//skip,
                                   interval=30, blit=True, repeat=False)
    anim.save(vis_path+'/nodes.mp4', dpi=80,
              progress_callback=lambda i, n: print(f'Saving frame {i} of {n}'))




# def spatial_animation(trial_path) :
#     matplotlib.use("Agg")
#     trial_name = trial_path.rsplit('/', 1)[-1]
#     vis_path = create_visualization_folder(trial_path)

#     frequently_times = np.load(trial_path+'/frequently_times.npy',allow_pickle=True)


#     r_pos = np.load(trial_path+'/robot_position.npy',allow_pickle=True)
#     l_pos = np.load(trial_path+'/light_position.npy',allow_pickle=True)
#     sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)

#     rx,ry,ra = r_pos[:,0],r_pos[:,1],r_pos[:,2]
#     lx,ly = l_pos[:,0],l_pos[:,1]

#     #segments,c,cmap = segment_data_to_colors(trial_path)
#     color_data,color_data_f,cmap = segment_data_to_colors(trial_path)
#     c = color_data

#     mapping = cm.get_cmap(cmap)
#     rgb = np.array([mapping(v) for v in c/max(c)])

#     fig = figure(figsize=(6,6))
#     rows,cols = 1,1
#     ax = subplot2grid((rows,cols),(0,0))
#     ax.set_aspect('equal')
#     ax.set_title('');
#     ax.set_xlabel('');
#     ax.set_ylabel('');
#     ax.set_xlim(-0.0,1.0);
#     ax.set_ylim(-0.0,1.0)
#     r = ax.scatter([],[],s=20)
#     light_circle = plt.Circle((0.5, 0.5), 0.05, fc='#ffff00',ec='k')
#     ROBOT_RADIUS = 0.04
#     robot_circle = plt.Circle((0.5, 0.5), ROBOT_RADIUS, fc='#333333',ec='k')
#     robot_line,  = plt.plot([0,0],[0,0],color='0.9',lw=1)

#     ittext = ax.text(-0.1,-0.11, "", bbox={'facecolor':'k', 'alpha':1.0, 'pad':3},
#                      transform=ax.transAxes, ha="left",color='w')

#     skip = 25

#     def init():
#         ax.add_patch(light_circle)
#         ax.add_patch(robot_circle)
#         return r,light_circle,robot_circle,robot_line

#     tail_length = 5000
#     def update_fig(frame_i) :
#         frame_i *= skip
#         a = max(0,frame_i-tail_length)
#         b = frame_i

#         r.set_offsets(r_pos[a:b,:2])
#         rgb[a:b,3] = linspace(0,1,b-a)
#         r.set_color(rgb[a:b,:])

#         light_circle.center = (lx[b],ly[b])
#         robot_circle.center = (r_pos[b,0],r_pos[b,1])

#         xo = r_pos[b,0] + np.cos(r_pos[b,2])*ROBOT_RADIUS
#         yo = r_pos[b,1] + np.sin(r_pos[b,2])*ROBOT_RADIUS
#         robot_line.set_data([r_pos[b,0],xo],[r_pos[b,1],yo])

#         ittext.set_text(f't={sample_times[frame_i]}')

#         return r,light_circle,robot_circle,robot_line,ittext

#     from matplotlib import animation
#     anim = animation.FuncAnimation(fig, update_fig, init_func=init,
#                                    frames=len(sample_times)//skip,
#                                    interval=15,#30,
#                                    blit=True, repeat=False)

#     anim.save(vis_path+'/spatial.mp4', dpi=80,
#               progress_callback=lambda i, n: print(f'Saving frame {i} of {n}'))
    
