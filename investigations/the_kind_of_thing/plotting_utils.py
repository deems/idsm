from pylab import *
import mde
import os,sys
import dill as pickle
from matplotlib import cm            

CMAP_VMIN = 0.0
CMAP_VMAX = 10.0

def wrapped_euclidean_points(p, q):
    TORUS_RADIUS = 1.0
    diff = np.abs(p - q)
    return np.linalg.norm(np.minimum(diff, TORUS_RADIUS - diff))


def create_visualization_folder(trial_folder) :
    VIS_FOLDER = trial_folder+'/visualizations'
    try:
        os.makedirs(VIS_FOLDER)
    except FileExistsError:
        print("The ANALYSIS dir already exists.")
    return VIS_FOLDER

def create_summary_folder(DATA_DIR) :
    SUMMARY_FOLDER = DATA_DIR+'/summary_visualizations'
    try:
        os.makedirs(SUMMARY_FOLDER)
    except FileExistsError:
        print("The SUMMARY visualizations dir already exists. ")
    return SUMMARY_FOLDER


import seaborn as sns
import matplotlib.pylab as plt
import numpy as np
from matplotlib.colors import ListedColormap

# construct cmap
# flatui = [
#     "#aaaaaa",
#     "#9b59b6",
#     "#95a5a6",
#     "#e74c3c",
#     "#34495e",
#     "#2ecc71",
#     "#b69b59",
#     "#9bb659",
#     "#345e49",
#     "#5e4934",
#     "#3498db",    
# ]
flatui = ["#aaaaaa","#a20021","#693d8f","#755c1b","#4d6cfa",
          "#247ba0","#f3752b","#f52f57","#cc9200","#515a47"]# https://coolors.co/a20021-693d8f-755c1b-4d6cfa-247ba0-f3752b-f52f57-cc9200-515a47-5f0a87


# flatui = ["#0000aa", #0
#           "#000000", #1
#           "#000000", #2
#           "#000000", #3
#           "#000000", #4
#           "#000000", #5
#           "#000000", #6
#           "#000000", #7
#           "#000000", #8
#           "#000000", #9
#           "#000000", #10
# ]
my_cmap = ListedColormap(sns.color_palette(flatui).as_hex())

# N = 500
# data1 = np.random.randn(N)
# data2 = np.random.randn(N)
# colors = np.linspace(0,1,N)
# plt.scatter(data1, data2, c=colors, cmap=my_cmap)
# plt.colorbar()
# plt.show()


def segment_data_to_colors(trial_path):
    #print(segments)
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)
    frequently_times = np.load(trial_path+'/frequently_times.npy',allow_pickle=True)
    color_data   = np.zeros_like(sample_times)
    color_data_f = np.zeros_like(frequently_times)

    #### Check to see if there is a human-made segmenting file that specifies
    #### how to color the plots
    try :
        with open(trial_path+'/segments.pkl', 'rb') as file:
            segments = pickle.load(file)['segments']


        for segment in segments :
            b,d = segment['b'],segment['d']
            bf,df = segment['bf'],segment['df']
            color_data[b:d] = segment['index']
            color_data_f[bf:df] = segment['index']

            ## scale indices to hit each individual color in map
            #cmap = cm.tab10
            #cmap = cm.gnuplot
            cmap = my_cmap
            N_COL_IN_CMAP = 10            

        # print(color_data)
        # color_data   *= 0.99 #N_COL_IN_CMAP/10.0#/max(color_data)
        # color_data_f *= 0.99 #N_COL_IN_CMAP/10.0#/max(color_data)

        # plot(color_data)
        # show()
        # quit()
    except FileNotFoundError:
        color_data   = sample_times
        color_data_f = frequently_times
        cmap = cm.gnuplot #'viridis','cividis',newcmp
    return color_data,color_data_f,cmap


## example usage of trim_spatial_data
# xsm,ysm,[alphasm,sample_timesm] = trim_spatial_data(xss,yss,
#                                                     [alphas,
#                                                      sample_times,
#                                                     ])


def trim_spatial_data(xs,ys,list_of_additional_arrays_to_mask=[]):
    dxdt = np.diff(xs)
    dydt = np.diff(ys) 
    mask = np.logical_or( abs(dxdt) > 0.1, abs(dydt) > 0.1 )
    print(np.shape(mask))
    xsm = np.ma.masked_where(mask, xs[:-1])
    ysm = np.ma.masked_where(mask, ys[:-1])
    masked_arrays = []
    for maskme in list_of_additional_arrays_to_mask :
        maskme = np.ma.masked_where(mask, maskme[:-1])
        masked_arrays.append(maskme)
    return xsm,ysm,masked_arrays


def data_to_nodes(trial_path):
    """ reads a data folder and translates node data arrays 
    into a list of NodeDictionaries. """

    # ## don't redo the work if its already been done and saved
    # if os.path.exists(os.path.join(trial_path,f'nodes.pkl')) :
    #     with open(os.path.join(trial_path,f'nodes.pkl'), 'rb') as file:
    #         B = pickle.load(file)
    #         return B           

    color_data,color_data_f,cmap = segment_data_to_colors(trial_path)
    
    frequently_its = np.load(trial_path+'/frequently_its.npy',allow_pickle=True)
    
    #closest_node_indices = np.load(trial_path+'/closest_node_index.npy',allow_pickle=True)
    node_locs = np.load(trial_path+'/node_locs.npy',allow_pickle=True)
    node_weights = np.load(trial_path+'/node_weights.npy',allow_pickle=True)

    ## normalize node weights to be between 0 and 1 (0 is off currently)
    node_weights[node_weights < 0.001] = 0.0
    node_weights /= np.max(node_weights)

    nodes_exist = node_weights > 0.0
    nodes_born = diff(nodes_exist,axis=0,prepend=0) ==  1.0
    nodes_die  = diff(nodes_exist,axis=0,prepend=0) == -1.0
    nodes_die[-1,:] = 1.0

    
    ## this is the data from the idsm weights tables.  It is to be
    ## split into individual node dictionaries.
    n_samples,n_cols = np.shape(node_weights)
    
    #### ## Debugging plots
    # subplot2grid((1,3),(0,0))
    # imshow(node_weights,aspect='auto',cmap='gray_r')
    # xlim(-1,n_cols);#ylim(0,200);
    # subplot2grid((1,3),(0,1))
    # imshow(nodes_born,aspect='auto',cmap='gray_r')
    # xlim(-1,n_cols);#ylim(0,200);
    # subplot2grid((1,3),(0,2))
    # imshow(nodes_die,aspect='auto',cmap='gray_r')
    # xlim(-1,n_cols);#ylim(0,200);

    ## column by column of the idsm table, this loop picks out each
    ## sequence of weights that corresponds to the lifetime of a
    ## single node. It extracts the info of that node and puts it into
    ## a dictionary which is added to the `nodes` list and eventually
    ## returned.
    nodes = []
    for ci in range(n_cols):
        ## times of these events
        births = np.argwhere(nodes_born[:,ci]==1)
        deaths = np.argwhere(nodes_die[:,ci]==1)
        # print(f'c#{ci}')
        for birth,death in zip(births,deaths):
            b = birth[0]
            d = death[0]
            b_i = frequently_its[b]
            d_i = frequently_its[d]
            # print(f'  {b} --> {d}')
            loc = node_locs[b][:,ci]
            weight_h = node_weights[b:d,ci]
            #plot(weight_h,label=f'{loc}')
            node = {
                'b' : b, # birth sample index (freq)
                'd' : d, # death sample index (freq)
                'b_i' : b_i, # birth iteration
                'd_i' : d_i, # death iteration
                'loc' : loc, # location in sms 
                'weight_h' : weight_h,#
                'original_col'  : ci,
                'segment_index' : color_data[b_i],
            }
            nodes.append( node )
            # print(node['b'])

    # sort nodes by birth sample index
    nodes = sorted(nodes,key = lambda x: x['b'])
    for n_i,n in enumerate(nodes):
        n['index'] = n_i        

    # ## generate a lookup table that is later used to translate the
    # ## simulation's 'closest_node_indices' to the new, more
    # ## informative node-indices (indexed by when they were created,
    # ## instead of arbitrary and reused indices)
    # actual_node_index_lut = np.zeros((n_samples,n_cols))
    # for n_i,n in enumerate(nodes):
    #     ci = n['original_col']
    #     actual_node_index_lut[n['b']:n['d'],ci] = n_i


    # ## closests node index is sampled every iteration, whereas the weights
    # ## are only sampled every `freq_rate`th iteration.
    # ## this factor you multiply by the closest node samples to figure out which
    # ## frequently sample is closest        
    # freq_rate = np.load(trial_path+'/frequently_its.npy',allow_pickle=True)[1]
    # def closest_freq_sample(it) :
    #     """takes an iteration # and returns the closest sample from the
    #     frequently sample times"""
    #     return int(floor(it/freq_rate))

    # ## look up the `correct` node indices so as to generate a correct
    # ## actual_closest_node_indices to be returned
    # sample_its = np.load(trial_path+'/sample_its.npy',allow_pickle=True)
    # actual_closest_node_indices = []
    # for it,closest_node_index in zip(sample_its,closest_node_indices) :
    #     if closest_node_index == None:
    #         closest_node_index = -1
    #     acni = actual_node_index_lut[closest_freq_sample(it),closest_node_index]
    #     actual_closest_node_indices.append(acni)

    with open(os.path.join(trial_path,f'nodes.pkl'), 'wb') as file:
        pickle.dump(nodes, file)
    
    return nodes#, np.array(actual_closest_node_indices)


