from kivy.config import Config
Config.set('kivy','log_level','info')
# Config.set('kivy','log_enable', '0')
Config.write()

from model import Model
from rvit.core import init_rvit,rvit_reconnect

if __name__ == '__main__':    
    m = Model()
    m.reset(0)
    r = 1
    init_rvit(m,rvit_file='rvit.kv',window_size=(1200/r,800/r))
