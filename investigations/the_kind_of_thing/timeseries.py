from pylab import *
import sys,os
import dill as pickle

from plotting_utils import *

def timeseries(trial_path,summary_path=None):
    trial_name = trial_path.rsplit('/', 1)[-1]

    vis_path = create_visualization_folder(trial_path)

    r_pos = np.load(trial_path+'/robot_position.npy',allow_pickle=True)
    l_pos = np.load(trial_path+'/light_position.npy',allow_pickle=True)
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)
    sms = np.load(trial_path+'/sensorimotor_state.npy',allow_pickle=True)
    # nodes,closest_node = data_to_nodes(trial_path)

    
    pickle_obj_path = os.path.join(trial_path,f'pickle_objs.pkl')
    print(pickle_obj_path)
    with open(pickle_obj_path, 'rb') as file:
        B = pickle.load(file)

    motor_state_to_motor_value = B['motor_state_to_motor_value']

    lxs,lys = l_pos[:,0],l_pos[:,1]
    xs,ys,alphas = r_pos[:,0],r_pos[:,1],r_pos[:,2]

    ## from robot to light
    dxs = np.array([l-r for l,r in zip(xs,lxs)]).ravel()
    dys = np.array([l-r for l,r in zip(ys,lys)]).ravel()

    #segments,c,cmap = segment_data_to_colors(trial_path)
    color_data,color_data_f,cmap = segment_data_to_colors(trial_path)

    scatter_args = {
        's' : 0.05,
        'cmap' : cmap,
        'c' : color_data,
        'alpha': 0.8,
        'vmin' : 0.0,
        'vmax' : 10.0,
    }

    def endplot(final_plot=False):
        xlim(0,max(sample_times))
        if not final_plot :
            xticks([])

    figure(figsize=(12,8))
    rows,cols = 5,1

    subplot2grid((rows,cols),(0,0))
    ds = np.sqrt(dxs**2 + dys**2)
    threequarters = int(3*len(ds)/4)
    print(f'mean ds: {np.mean(ds[threequarters:])}')
    print(f'std  ds: {np.std(ds[threequarters:])}')
    scatter(sample_times,ds,**scatter_args)
    ylabel('distance between\nrobot and light')
    endplot()

    subplot2grid((rows,cols),(1,0))
    scatter(sample_times,motor_state_to_motor_value(sms[:,2]),**scatter_args)
    ylim(-1.1,1.1)
    ylabel('left\nmotor')
    endplot()

    subplot2grid((rows,cols),(2,0))
    scatter(sample_times,motor_state_to_motor_value(sms[:,3]),**scatter_args)
    ylim(-1.1,1.1)
    ylabel('right\nmotor')
    endplot()

    subplot2grid((rows,cols),(3,0))
    scatter(sample_times,sms[:,0],**scatter_args)
    ylim(0,1.1)
    ylabel('left\nsensor')
    endplot()

    subplot2grid((rows,cols),(4,0))
    scatter(sample_times,sms[:,1],**scatter_args)
    ylim(0,1.1)
    ylabel('right\nsensor')
    endplot(final_plot=True)

    # subplot2grid((rows,cols),(5,0))
    # scatter(sample_times,closest_node,**scatter_args)
    # # colorbar()
    # ## super impose time series showing the closest node (in red)
    # # plot(sample_times,closest_node,'k,',ms=0.01,alpha=0.1)
    # endplot(final_plot=True)

    tight_layout()
    savefig(vis_path+f'/timeseries.png',dpi=300)
    if summary_path is not None :
        savefig(summary_path+'/'+f'timeseries_{trial_name}.png',dpi=300)
    close()
    # show()
