import matplotlib
from pylab import *
from matplotlib.backend_bases import MouseButton

from mpl_toolkits.mplot3d import Axes3D

import dill as pickle
import os,sys,tempfile
import glob

# from model import Model

from celluloid import Camera

from scatter_plots import scatter_plots
from timeseries import timeseries
from plot_node_histories import plot_node_histories
from animations import *

from scipy.spatial.distance import cdist
from preprocess_data import calculate_node_distance_history, calculate_hires_closest_node_sequence
from plotting_utils import *

#DATA_DIR = '/home/mde/Desktop/idsm_data_hearing_vision/idsmtrials_normal/'
DATA_DIR = '/home/mde/Desktop/idsm_data_hearing_vision/idsmtrials_hearing/'

m = 0.8
clist = [
    (0,0,0),
    (m,0,0),
    (0,m,0),
    (m,m,0),
    (0,0,m),
    (m,0,m),
    (0,m,m),
    (m,m,m),    
    ]

b = 0
bf = 0
d = 0
df = 0

segments=[]
selected_segment = None

def save_segments(savedir):
    pickleme = {
        'segments' : segments,
    }
    
    with open(os.path.join(savedir,f'segments.pkl'), 'wb') as file:
        pickle.dump(pickleme, file)



        
def annotate(trial_path):
    global segments,colors

    ## load preexisting segments if any
    try :
        with open(trial_path+'/segments.pkl', 'rb') as file:
            segments = pickle.load(file)['segments']
    except FileNotFoundError:
        segments = []

    trial_name = trial_path.rsplit('/', 1)[-1]
    vis_path = create_visualization_folder(trial_path)

    fig = figure(figsize=(16,4.2))    
    nodes = data_to_nodes(trial_path)

    frequently_times = np.load(trial_path+'/frequently_times.npy',allow_pickle=True)
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)

    dist = np.load(os.path.join(trial_path,'node_distance_history.npy'),allow_pickle=True)
    dist = dist.astype('float32') 
    no_node_mask = dist==-1.0 ## this is where there is no node
    dist[no_node_mask] = 1000000 ## where there is no node, make distance seem very very large

    threshold = 0.0001
    k_d = 500
    p = 2.0/(1.0+exp(k_d*dist**2)) > threshold

    # closestppars = {
    #     'alpha': 0.8,
    #     'ms' : 0.3,
    # }

    
    closest_nodes_rows = np.argmin(dist,axis=0)
    xs = frequently_times
    ys = closest_nodes_rows
    splot = scatter(xs,ys,s=0.5)
    xlim(0,max(frequently_times))
    ylim(0,max(closest_nodes_rows))

    def update_colors():
        global colors,segments
        colors = np.array( [(0.,0.,0.),]*len(closest_nodes_rows) )
        for segment in segments:
            bf = segment['bf']
            df = segment['df']
            colors[bf:df] = clist[segment['index']%8]
        splot.set_color(colors)

    
    ## highlight closest nodes    
    colors = np.array( [(0.,0.,0.),]*len(closest_nodes_rows) )
    update_colors()


    #redraw()


    def onclick(event):
        global b,d,bf,df,colors,segments,selected_segment
        print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
              ('double' if event.dblclick else 'single', event.button,
               event.x, event.y, event.xdata, event.ydata))
        time = event.xdata
        sample = int(time * len(sample_times) // max(sample_times))
        freq_sample = int(time * len(frequently_times) // max(frequently_times))
        # print(time,sample,freq_sample)

        if event.button == MouseButton.LEFT :
            ## if a segment is selected check if this is to the left of it
            ## if it is start a new segment that butts up against
            if selected_segment is not None:
                if freq_sample < selected_segment['bf']:
                    d = selected_segment['b']
                    df = selected_segment['bf']
                    bf = freq_sample
                    b = sample
                    selected_segment = None
                else :
                    b = d = sample
                    bf = df = freq_sample
                    selected_segment = None
            else :
                ## nothing is selected
                b = sample
                bf = freq_sample                

        if event.button == MouseButton.RIGHT :
            ## if a segment is selected check if this is to the right of it
            ## if it is, start a new segment that butts up against
            if selected_segment is not None:
                if freq_sample > selected_segment['df']:
                    b = selected_segment['d']
                    bf = selected_segment['df']
                    d = sample
                    df = freq_sample                    
                    selected_segment = None
                else :
                    b = d = sample
                    bf = df = freq_sample
                    selected_segment = None
            else :
                ## nothing is selected
                d = sample
                df = freq_sample
            
        if event.button == MouseButton.MIDDLE :
            clickx = freq_sample
            matching_segments = [segment for segment in segments if (segment['bf'] < clickx and segment['df'] >= clickx)]
            if len(matching_segments) > 0 :
                selected_segment = matching_segments[0]
            print(selected_segment)
            print(segments)

        update_colors()
        colors[bf:df] = (0.5,0.5,0.5)
        splot.set_color(colors)

        # plt.clf()
        # redraw()
        plt.draw() #redraw

    def onkey(event):
        global b,f,bf,df,segments,selected_segment
        if event.key == 'escape' :
            save_segments(trial_path)
            quit()
        # print('you pressed', event.key)
        color_index = int(event.key,16)
        if selected_segment is not None :
            selected_segment['index'] = color_index
        else :
            new_segment = {
                'b' : b,
                'd' : d,
                'bf': bf,
                'df': df,
                'index':color_index,
            }
            selected_segment = new_segment
            segments.append(new_segment)

        print(segments)

        update_colors()        
        plt.draw()               

    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    cid = fig.canvas.mpl_connect('key_press_event', onkey)

    show()
    close()   






#all_trial_trial_paths = sorted(glob.glob(DATA_DIR+'[0-9]*'))
all_trial_trial_paths = sorted(glob.glob(DATA_DIR+'0'))

# summary_path = create_summary_folder(DATA_DIR)
# for trial_path in all_trial_trial_paths[:]:
#     print(f'ANNOTATING {trial_path}')

#     annotate(trial_path)

#f = '/home/mde/Desktop/idsm_data_hearing_vision/idsmtrials_normal/9/'
f = '/home/mde/Desktop/idsm_data_hearing_vision/idsmtrials_hearing/0/'
annotate(f)

# legend()
# show()
