from pylab import *
from mpl_toolkits.mplot3d import Axes3D
from collections import Counter
from scipy.spatial.distance import cdist


# from graphviz import Digraph
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
from networkx.algorithms import community


from plotting_utils import *

import os,sys
import glob

from model import Model

DATA_DIR = '/home/mde/Desktop/idsmtrials_short/'
all_trial_folders = glob.glob(DATA_DIR+'0')

comcolors = ['red','green','blue','yellow','cyan','pink','purple']

def remove_consecutive_duplicates(from_me) :
    from itertools import groupby
    res = [i[0] for i in groupby(from_me)]
    return res


def sequence_to_graph(folder,draw=False) :
    sequence = np.load(folder+'/hires_node_distance_history.npy') ## needs to be updated
    
    s = remove_consecutive_duplicates( sequence )

    g = nx.DiGraph()
    nodes = set(s)
    node_counter = Counter( sequence ) ## pre consequtive duplicate removal

    transitions = [(s[i],s[i+1]) for i in range(len(s)-2)]
    transition_counter = Counter( transitions )

    print(f'#nodes: {len(nodes)}\t#transitions: {len(transitions)}')

    ## create edges to represent the transitions. The weights of these
    ## edges is proportional to the number of times its transition is
    ## transited
    for transition in set(transitions) :
        if transition_counter[transition] >= 1 :
            if transition[0] is not None and transition[1] is not None :
                weight = transition_counter[transition]
                g.add_edge(str(transition[0]),str(transition[1]),weight=weight)

    if draw :
        ## p is a `pygraphvis` graph (which has more plotting versatility)
        p=nx.nx_agraph.to_agraph(g)
        ## some default values
        p.node_attr['shape']='circle'
        p.node_attr['style'] = 'filled'
        p.node_attr['width'] = '0.2'
        p.node_attr['height'] = '0.2'
        p.node_attr['fixedsize'] = 'true' ## forces size to mean something

        for n in p.nodes():
            #n.attr['fillcolor'] = f'1.0 0.0 {visited_normalized}'
            n.attr['label'] = ''#node_counter[float(n)]

        p.write('graph.dot')
        p.draw('graph.png',prog='neato')

    with open(os.path.join(folder,f'transition_graph.p'), 'wb') as file:        
        pickle.dump(g,file)
        
        

for folder in all_trial_folders:
    print(folder)
    sequence_to_graph(folder)
    #calculate_hires_node_distance_history(folder)

show()
