from pylab import *
import dill as pickle
import numpy as np
import os,sys


class TrackedValue(object):
    def __init__(self, name:str, getfn:str, should_sample):
        """
        ARGS:
            name : a machine readable name of the data being sampled
                   used as the filename of the data being stored

            getfn : a str that will be evaluated to gather the sample
                    it will use `m` to refer to a base class from which
                    the data can be gathered, e.g. m.idsm.all_nodes

            should_sample : a function fn(m) that takes the iteration
                            number and model and returns True iff it is
                            a good time to eval getfn.
        """
        self.name = name
        self.getfn = getfn
        self.should_sample = should_sample
        self.data = []

    def iterate(self,m):
        from copy import deepcopy
        if self.should_sample(m) :
            self.data.append( deepcopy(eval(self.getfn)) )

class TrackingManager(object):
    def __init__(self) :
        self.trackers = []
        self.pickle_objs = {}

    def track(self,name,getfn,should_sample=lambda m:True) :
        self.trackers.append( TrackedValue(name,getfn,should_sample) )

    def add_pickle_obj(self,name,obj):
        self.pickle_objs[name] = obj

    def iterate(self,m):
        for t in self.trackers:
            t.iterate(m)

    def save(self,folder_path):
        path = folder_path
        try:
            os.makedirs(path)
        except FileExistsError:
            print("The data save dir already exists. "+
                  "Hopefully you're overwriting any "+
                  "previous data!\n")

        try:
            os.remove('latest_output')
        except FileNotFoundError:
            print('no latest_output file to remove')
        os.symlink(path,'latest_output')

        for t in self.trackers:
            np.save(os.path.join(folder_path,f'{t.name}.npy'),t.data,allow_pickle=True)

        with open(os.path.join(folder_path,f'pickle_objs.pkl'), 'wb') as file:
            pickle.dump(self.pickle_objs, file)
