(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("frontiersSCNS" "utf8")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("setspace" "onehalfspacing")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "frontiersSCNS"
    "frontiersSCNS10"
    "url"
    "hyperref"
    "lineno"
    "microtype"
    "subcaption"
    "setspace"
    "xspace")
   (TeX-add-symbols
    '("mde" 1)
    '("vect" 1)
    '("pns" 0)
    '("pn" 0)
    '("deriv" 1)
    '("marginnote" 1)
    "ie"
    "Ie"
    "eg"
    "Eg"
    "cf"
    "etAl"
    "twod"
    "threed"
    "oo"
    "ooo"
    "figlabelsymbol"
    "nodepos"
    "nodevel"
    "nodeweight"
    "keyFont"
    "firstAuthorLast"
    "Authors"
    "Address"
    "corrAuthor"
    "corrEmail")
   (LaTeX-add-labels
    "eq:robot_motion"
    "eq:light_excitation"
    "eq:light"
    "eq:ass_to_ss"
    "fig:lambda"
    "eq:distance_fn"
    "eq:familiarity"
    "eq:weight_to_omega_fn"
    "eq:weight_ODE"
    "eq:rejuvenation_term"
    "eq:change_in_motor"
    "eq:component_removal"
    "eq:random_combination"
    "tab:parameters")
   (LaTeX-add-bibliographies
    "test"))
 :latex)

