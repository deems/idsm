(TeX-add-style-hook
 "pre_frontiers"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt" "")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("xcolor" "usenames" "dvipsnames") ("apacite" "natbibapa") ("mathpazo" "osf" "sc") ("hyperref" "hidelinks") ("enumitem" "inline" "shortlabels")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "relsize"
    "theorem"
    "color"
    "colortbl"
    "shadethm"
    "xcolor"
    "soul"
    "booktabs"
    "multirow"
    "epsfig"
    "wrapfig"
    "xspace"
    "graphicx"
    "apacite"
    "amsmath"
    "amsfonts"
    "amssymb"
    "titling"
    "fancyhdr"
    "mathpazo"
    "overpic"
    "textcomp"
    "float"
    "hyperref"
    "enumitem"
    "geometry")
   (TeX-add-symbols
    '("ploscaption" 2)
    '("jpm" 1)
    '("mde" 1)
    '("vect" 1)
    '("pns" 0)
    '("pn" 0)
    '("deriv" 1)
    '("marginnote" 1)
    "captionfonts"
    "imgscale"
    "ie"
    "Ie"
    "eg"
    "Eg"
    "cf"
    "etAl"
    "twod"
    "threed"
    "true"
    "false"
    "viableregion"
    "nonviableregion"
    "nSamples"
    "VRegion"
    "Vregion"
    "vregion"
    "vulnerability"
    "Vulnerability"
    "figlabelsymbol"
    "nodepos"
    "nodevel"
    "nodeweight"
    "maxx"
    "maxa")
   (LaTeX-add-labels
    "eq:robot_motion"
    "eq:light_excitation"
    "eq:light"
    "eq:ass_to_ss"
    "fig:lambda"
    "eq:distance_fn"
    "eq:familiarity"
    "eq:weight_to_omega_fn"
    "eq:weight_ODE"
    "eq:rejuvenation_term"
    "eq:change_in_motor"
    "eq:component_removal"
    "eq:random_combination"
    "tab:parameters")
   (LaTeX-add-bibliographies
    "zotero.bib")
   (LaTeX-add-color-definecolors
    "shadethmcolor"
    "shaderulecolor"))
 :latex)

