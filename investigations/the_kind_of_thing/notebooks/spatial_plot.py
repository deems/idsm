from pylab import *

scatter(data['r_pos'][:,0],
        data['r_pos'][:,1],
        c=data['colors'],alpha=0.5,cmap=data['cmap'],s=5);
gca().set_aspect('equal')

