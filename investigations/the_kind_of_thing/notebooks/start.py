from IPython import get_ipython
ipython = get_ipython()
import dill as pickle

import sys
sys.path.insert(0,'..')
ipython.magic("load_ext autoreload")
ipython.magic("%autoreload 2")

from plotting_utils import *


def load_trial(trial_path) :
    trial_data = {}
    
    pickle_obj_path = os.path.join(trial_path,f'pickle_objs.pkl')

    with open(pickle_obj_path, 'rb') as file:
        B = pickle.load(file)

    motor_state_to_motor_value = B['motor_state_to_motor_value']
    
    r_pos = np.load(trial_path+'/robot_position.npy',allow_pickle=True)
    l_pos = np.load(trial_path+'/light_position.npy',allow_pickle=True)
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)
    sms = np.load(trial_path+'/sensorimotor_state.npy',allow_pickle=True)
    lxs,lys = l_pos[:,0],l_pos[:,1]
    xs,ys,alphas = r_pos[:,0].ravel(),r_pos[:,1].ravel(),r_pos[:,2].ravel()

    trial_data['r_pos'] = r_pos
    trial_data['r_x']  = r_pos[:,0]
    trial_data['r_y']  = r_pos[:,1]
    trial_data['r_a']  = r_pos[:,2]

    trial_data['l_pos'] = l_pos
    trial_data['l_x']   = l_pos[:,0]
    trial_data['l_y']   = l_pos[:,1]
    
    trial_data['sample_times'] = sample_times
    trial_data['sms'] = sms

    segments,colors,cmap = segment_data_to_colors(trial_path)
    trial_data['segments'] = segments
    trial_data['colors']   = colors
    trial_data['cmap']     = cmap
    

    return trial_data
