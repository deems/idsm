from pylab import *
import sys,os
import dill as pickle

#from plotting_utils import *

trial_path = '/home/mde/Desktop/idsmtrials/0/'

pickle_obj_path = os.path.join(trial_path,f'pickle_objs.pkl')
with open(pickle_obj_path, 'rb') as f:
    B = pickle.load(f)

motor_state_to_motor_value = B['motor_state_to_motor_value']

x=linspace(0,1,1001)
fig=figure(figsize=(6,2.0))
plot(x,motor_state_to_motor_value(x),'k-',lw=2)
xlim(0,1)
ylim(-1.1,1.1)
xlabel('$x$')
ylabel('$\Lambda(x)$')
tight_layout()
savefig('/home/mde/projects/7_frontiers/img/lambda.png',dpi=300)
#show()

