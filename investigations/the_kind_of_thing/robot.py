from pylab import *
from scipy.spatial.distance import euclidean,cdist

def wrapped_euclidean_points(p, q):
    TORUS_RADIUS = 1.0
    diff = np.abs(p - q)
    return np.linalg.norm(np.minimum(diff, TORUS_RADIUS - diff))

def circular_distance(a,b) :
    """this assumes the arena is between -1 and 1"""
    a = (a + 1.0) / 2.0
    b = (b + 1.0) / 2.0

    return min(abs(a-b),abs(1.0-abs(a-b)))

class Light(object) :
    def __init__(self) :
        self.x = np.array([0.5])
        self.y = np.array([0.5])

        self.reset()

    def reset(self) :
        self.x[:] = 0.5
        self.y[:] = 0.5


    def impact_sensor(self,sensor_x,sensor_y,sensor_angle) :
        offsets = [[-1.,0.],
                   [+1.,0.],
                   [0.,-1.],
                   [0.,+1.],
                   [0.,0.],
                   [-1.,-1.],
                   [-1.,+1.],
                   [+1.,-1.],
                   [+1.,+1.]]

        lpos = np.array( [[self.x + lox, self.y + loy] for lox,loy in offsets] ).reshape(9,2)
        spos = np.array([sensor_x,sensor_y]).reshape(1,2) # np.array([0.1,0.1]).reshape(1,2) #

        # calculate attenuation due to directionality
        # sensor to light unit vector
        s2l = lpos - spos

        distances = linalg.norm(s2l,axis=1)

        # if the sensor were omnidirectional, its value would be
        falloff = 0.25 # lower number, sensors fall off faster
        omni = falloff/(falloff+distances**2)

        dstacked = np.dstack([distances,
                              distances])

        # normalized s2l
        s2l = np.where(dstacked>0.0,s2l/dstacked,0).reshape(9,2)

        # sensor direction unit vector
        sd = np.array([np.cos(sensor_angle),
                       np.sin(sensor_angle)]).reshape(1,2)

        # positive set of dot product
        attenuation = sd @ s2l.T
        attenuation[attenuation<0.0] = 0.0


        combined = np.max(omni*attenuation)
        return combined


class Sound(object) :
    def __init__(self) :
        print('USING SOUND NOT LIGHT!!!')
        self.x = np.array([0.5])
        self.y = np.array([0.5])
        self.prev_distances = {}
        self.reset()

    def reset(self) :
        self.x[:] = 0.5
        self.y[:] = 0.5


    def impact_sensor(self,sensor_x,sensor_y,sensor_angle,sensor_name,DT) :
        offsets = [[-1.,0.],
                   [+1.,0.],
                   [0.,-1.],
                   [0.,+1.],
                   [0.,0.],
                   [-1.,-1.],
                   [-1.,+1.],
                   [+1.,-1.],
                   [+1.,+1.]]

        lpos = np.array( [[self.x + lox, self.y + loy] for lox,loy in offsets] ).reshape(9,2)
        spos = np.array([sensor_x,sensor_y]).reshape(1,2) # np.array([0.1,0.1]).reshape(1,2) #

        # calculate attenuation due to directionality
        # sensor to light unit vector
        s2l = lpos - spos

        ## according to minimal representation of periodic boundaries, the distance
        ## between the light and this sensor is lsd
        distances = linalg.norm(s2l,axis=1)
        lsd = min(distances)

        if sensor_name not in self.prev_distances.keys() :
            self.prev_distances[sensor_name] = lsd

        # change in distance for this sensor
        dd = (lsd - self.prev_distances[sensor_name])/DT

        ## save current distance for next check
        self.prev_distances[sensor_name] = lsd

        return(0.5+dd*0.75)

        # # # if the sensor were omnidirectional, its value would be
        # # falloff = 0.25 # lower number, sensors fall off faster
        # # omni = falloff/(falloff+distances**2)

        # # dstacked = np.dstack([distances,
        # #                       distances])

        # # # normalized s2l
        # # s2l = np.where(dstacked>0.0,s2l/dstacked,0).reshape(9,2)

        # # # sensor direction unit vector
        # # sd = np.array([np.cos(sensor_angle),
        # #                np.sin(sensor_angle)]).reshape(1,2)

        # # # positive set of dot product
        # # attenuation = sd @ s2l.T
        # # attenuation[attenuation<0.0] = 0.0


        # # combined = np.max(omni*attenuation)
        # return 0.0


class Robot(object) :
    def __init__(self) :
        self.RADIUS = 0.05

        self.it = 0
        self.pos = np.array([0.0,0.0,0.0])

        self.history_length = 1000

        self.pos_history = np.random.rand(self.history_length,3)

        self.lm = np.array([0.0])
        self.rm = np.array([0.0])

        self.lights = []

        self.reset()

    def reset(self) :
        self.pos[:] = [0.0,0.0,0.0]

        ## motors
        self.lm[:] = 0.0
        self.rm[:] = 0.0

        ## sensors
        self.ls = 0.0
        self.rs = 0.0

        self.t = 0.0
        self.DT = 0.01

    def randomizeState(self) :
        self.pos[:] = [np.random.rand(),np.random.rand(),np.random.rand()*2.0*np.pi]

    def updateSensors(self) :
        ## calculate sensor positions
        #point_between_the_eyes
        pbte = 0 #np.pi/2 ## in a normal run, this is 0

        beta = np.pi / 5.0 ## default is for sight
        if isinstance(self.lights[0],Sound)  :
            # in this case we are using ears on the sides of the head 
            beta = np.pi / 2.0
            
        x,y,a = self.pos

        a+=pbte

        lsx = x + cos(a+beta)*self.RADIUS
        lsy = y + sin(a+beta)*self.RADIUS
        lsa = a + beta
        rsx = x + cos(a-beta)*self.RADIUS
        rsy = y + sin(a-beta)*self.RADIUS
        rsa = a - beta

        ## calculate light impacts on sensors
        self.ls = self.rs = 0.0


        if isinstance(self.lights[0],Sound)  :
            for sound in self.lights :
                self.ls += sound.impact_sensor(lsx,lsy,lsa,'leftear',self.DT)
                self.rs += sound.impact_sensor(rsx,rsy,rsa,'rightear',self.DT)
        else :
            for light in self.lights :
                self.ls += light.impact_sensor(lsx,lsy,lsa)
                self.rs += light.impact_sensor(rsx,rsy,rsa)

    def iterate(self) :
        x,y,a = self.pos
        self.pos_history[self.it%self.history_length,:] = x,y,a

        self.updateSensors()

        self.it += 1
        self.t += self.DT

        speed = 0.25
        self.dx = speed * cos(a)*(self.lm+self.rm)
        self.dy = speed * sin(a)*(self.lm+self.rm)
        self.da = speed * (self.rm-self.lm) / (2.0*self.RADIUS)

        self.pos[0] += self.DT * self.dx
        self.pos[1] += self.DT * self.dy
        self.pos[2] += self.DT * self.da

        if self.pos[0] > 1.0 :
            self.pos[0] -= 1.0
        if self.pos[0] < -0.0 :
            self.pos[0] += 1.0
        if self.pos[1] > 1.0 :
            self.pos[1] -= 1.0
        if self.pos[1] < -0.0 :
            self.pos[1] += 1.0


def test_directional_light_sensors() :
    r = Robot()
    l = Light()
    l.x[:] = 0.5
    l.y[:] = 0.5
    r.lights.append(l)

    res = linspace(0,1,50)
    xs,ys = mesh = meshgrid(res,res)

    def f(coords) :
        r.pos[0] = coords[0]
        r.pos[1] = coords[1]
        r.pos[2] = 0.0
        r.updateSensors()
        return r.ls,r.rs

    zs = apply_along_axis(f,0,mesh)
    subplot2grid((1,2),(0,0))
    imshow(zs[0],origin='lower',extent=[0,1,0,1])
    title('left sensor')
    subplot2grid((1,2),(0,1))
    imshow(zs[1],origin='lower',extent=[0,1,0,1])
    title('right sensor')
    show()

if __name__ == '__main__':
    test_directional_light_sensors()
