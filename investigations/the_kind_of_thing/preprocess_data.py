import matplotlib
from pylab import *

from mpl_toolkits.mplot3d import Axes3D

import dill as pickle
import os,sys,tempfile
import glob

# from model import Model

from celluloid import Camera

from scatter_plots import scatter_plots
from timeseries import timeseries
from plot_node_histories import plot_node_histories
from animations import *

from scipy.spatial.distance import cdist

from plotting_utils import *

def calculate_node_distance_history(trial_path,summary_path=None):
    trial_name = trial_path.rsplit('/', 1)[-1]
    vis_path = create_visualization_folder(trial_path)

    nodes = data_to_nodes(trial_path)
    sms = np.load(trial_path+'/sensorimotor_state.npy',allow_pickle=True)

    frequently_times = np.load(trial_path+'/frequently_times.npy',allow_pickle=True)
    frequently_its = np.load(trial_path+'/frequently_its.npy',allow_pickle=True)
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)
    
    ## this matrix is -1 when there is no node and otherwise shows the distance
    ## between the jth node at the ith sample-time
    distance_heatmap = np.ones((len(frequently_times),len(nodes)))*-1
    for n_i,n in enumerate(nodes):
        b = n['b']
        d = n['d']
        b_i = n['b_i']
        d_i = n['d_i']
        skip = frequently_its[1]
        
        ## this node's position XA 1,4
        npos = np.array(n['loc']).reshape(1,4)
        ## the sms history during the node's life XB N,4
        smsh = sms[b_i:d_i:skip,:]
        dists = cdist(npos,smsh,metric=wrapped_euclidean_points)

        # if n_i == 1 :
        #     #### sanity check
        #     print(f'The relevant iteration range is from {b_i} to {d_i}.')
        #     print(f'The smsh is sampled every iteration.')
        #     print(f'The nodes are sampled every {skip} iterations.')
        #     print(f'I am comparing the SMS at iteration {d_i} to the {d}th node sample.')

        #     plot(sample_times[b_i:d_i:skip],smsh[:,0],label='sms[0]')
        #     plot(frequently_times[b:d],dists[0],label='dist to sms')
        #     plot(frequently_times[b:d],n['weight_h'],label='w_h')
        #     legend()
        #     show()
        #     quit()
        distance_heatmap[b:d,n_i] = dists[0]

    ## show node histories (weights change over time)    
    prox = distance_heatmap.T
    proxfilename = os.path.join(trial_path,'node_distance_history.npy')
    np.save(proxfilename,prox.data,allow_pickle=True)

def calculate_hires_closest_node_sequence(trial_path,summary_path=None):
    trial_name = trial_path.rsplit('/', 1)[-1]
    vis_path = create_visualization_folder(trial_path)

    nodes = data_to_nodes(trial_path)
    sms = np.load(trial_path+'/sensorimotor_state.npy',allow_pickle=True)

    frequently_times = np.load(trial_path+'/frequently_times.npy',allow_pickle=True)
    frequently_its = np.load(trial_path+'/frequently_its.npy',allow_pickle=True)
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)
    sample_its = np.load(trial_path+'/sample_its.npy',allow_pickle=True)

    ## this matrix is -1 when there is no node and otherwise shows the distance
    ## between the jth node at the ith sample-time
    #distance_heatmap = np.ones((len(sample_times),len(nodes)))*-1
    closest_nodes_hires = []
    for it,sm_state in zip(sample_its,sms):
        alive_nodes = [n for n in nodes if it >= n['b_i'] and it < n['d_i']]
        if len(alive_nodes) > 0 :
            alive_node_locs  = np.array( [n['loc'] for n in alive_nodes] )
            dists = cdist(alive_node_locs,sm_state.reshape(1,4),metric=wrapped_euclidean_points)
            closest_index = np.argmin(dists)
            # print(closest_index)
            closest_node_index = alive_nodes[closest_index]['index']
            closest_nodes_hires.append(closest_node_index)
            print(f'{100.0*it/len(sample_its)}% done', sep=' ', end='\r', flush=True)

    plot(closest_nodes_hires,'.')
    ## show node histories (weights change over time)
    filename = os.path.join(trial_path,'hires_closest_node_sequence.npy')
    np.save(filename,np.array(closest_nodes_hires).data,allow_pickle=True)
