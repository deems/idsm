import matplotlib
from pylab import *

from mpl_toolkits.mplot3d import Axes3D

import dill as pickle
import os,sys,tempfile
import glob

# from model import Model

from celluloid import Camera

from scatter_plots import scatter_plots
from timeseries import timeseries
from plot_node_histories import plot_node_histories
from animations import *

from scipy.spatial.distance import cdist
from preprocess_data import calculate_node_distance_history, calculate_hires_closest_node_sequence
from plotting_utils import *

#DATA_DIR = '/home/mde/Desktop/idsmtrials/'
#all_trial_folders = sorted(glob.glob(DATA_DIR+'[0-9]*'))

DATA_DIR = '/home/mde/Desktop/idsm_data_hearing_vision/idsmtrials_normal/'
all_trial_folders = sorted(glob.glob(DATA_DIR+'9'))

# DATA_DIR = '/home/mde/Desktop/idsm_data_hearing_vision/idsmtrials_hearing/'
# all_trial_folders = sorted(glob.glob(DATA_DIR+'0'))

summary_path = create_summary_folder(DATA_DIR)
for folder in all_trial_folders[:]:
    trial_path = folder
    print(f'ANALYZING {trial_path}')

    # DEPRECATED calculate_hires_closest_node_sequence(folder) ## only used in graph anaylssi at this stage

    # # ## preprocess data    
    # calculate_node_distance_history(folder)
    
    # # # # ## generate plots
    scatter_plots(trial_path,summary_path)
    # timeseries(trial_path,summary_path)
    # plot_node_histories(trial_path,summary_path) 
    
    # # spatial_animation(trial_path) spatial is captured in the other too, so...
    # animation(trial_path)
    # # quit()

#legend()
#show()
