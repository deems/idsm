from pylab import *
import sys,os
import dill as pickle

from plotting_utils import *

def scatter_plots(trial_path,summary_path=None) :
    trial_name = trial_path.rsplit('/', 1)[-1]
    vis_path = create_visualization_folder(trial_path)
    
    print(f'GENERATING SCATTER PLOTS FOR {trial_name}')

    pickle_obj_path = os.path.join(trial_path,f'pickle_objs.pkl')
    print(pickle_obj_path)
    with open(pickle_obj_path, 'rb') as file:
        B = pickle.load(file)

    motor_state_to_motor_value = B['motor_state_to_motor_value']
    
    r_pos = np.load(trial_path+'/robot_position.npy',allow_pickle=True)
    l_pos = np.load(trial_path+'/light_position.npy',allow_pickle=True)
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)
    sms = np.load(trial_path+'/sensorimotor_state.npy',allow_pickle=True)
    lxs,lys = l_pos[:,0],l_pos[:,1]
    xs,ys,alphas = r_pos[:,0].ravel(),r_pos[:,1].ravel(),r_pos[:,2].ravel()

    scatter_args = {
        'alpha' : 0.5,
        's' : 0.5,
    }        

    color_data,color_data_f,cmap = segment_data_to_colors(trial_path)
    scatter_args['c'] = color_data
    scatter_args['cmap'] = cmap
    scatter_args['vmin'] = 0.0
    scatter_args['vmax'] = 10.0

    def newplot(figsize=(5,5)):
        figure(figsize=figsize)
        gca().set_aspect('equal')

    def endplot(name,copy_to_summary=True):
        tight_layout()
        savefig(vis_path+f'/{name}.png')
        if copy_to_summary and summary_path is not None :
            savefig(summary_path+'/'+f'{name}_{trial_name}.png')
        print(f'{name} completed')
        close()


    for habit_color in set(color_data) :
        newplot()
        habit_mask = [color_data==habit_color]
        masked_pos = r_pos[habit_mask]
        sa = dict(scatter_args)
        sa['c'] = [habit_color,]*len(masked_pos)
        # sa['c'][0] = 0.0
        # sa['c'][-1] = 10.0
        scatter(masked_pos[:,0],masked_pos[:,1],**sa)
        #xlabel('x');ylabel('y');title("Robot Position");
        xticks([]);yticks([])
        xlim(0,1);ylim(0,1)
        endplot(f'robot_position_habit_{int(habit_color)}',copy_to_summary=False)

    for habit_color in set(color_data) :
        newplot(figsize=(20,5))        
        habit_mask = [color_data==habit_color]
        print(np.shape(habit_mask))
        print(np.shape(sms))
        masked_sms = sms[habit_mask]
        sa = dict(scatter_args)
        sa['c'] = [habit_color,]*len(masked_sms)
        # sa['c'][0] = 0.0
        # sa['c'][-1] = 10.0

        subplot2grid((1,4),(0,0))
        scatter(masked_sms[:,0],masked_sms[:,1],**sa)
        xlim(0,1);ylim(0,1)
        xlabel('ls');ylabel('rs');
        
        # subplot2grid((1,4),(0,1))
        # scatter(masked_sms[:,3],masked_sms[:,2],**sa)
        # xlim(0,1);ylim(0,1)
        # xlabel('SMS.lm');ylabel('SMS.rm');
        subplot2grid((1,4),(0,1))
        scatter(motor_state_to_motor_value(masked_sms[:,2]),
                motor_state_to_motor_value(masked_sms[:,3]),**sa)
        xlim(-1,1);ylim(-1,1)
        xlabel('left motor');ylabel('right motor');
        
        # subplot2grid((1,4),(0,2))
        # scatter(masked_sms[:,0]-masked_sms[:,1],
        #         masked_sms[:,2]-masked_sms[:,3],**sa) 
        # xlabel('ls-rs');ylabel('SMS.lm-SMS.rm');
        # xlim(-1,1)
        # ylim(-1,1)

        subplot2grid((1,4),(0,2))
        scatter(masked_sms[:,0],
                motor_state_to_motor_value(masked_sms[:,2]),**sa) 
        xlabel('left sensor');ylabel('left motor');
        xlim(0,1)
        ylim(-1,1)

        subplot2grid((1,4),(0,3))
        scatter(masked_sms[:,1],
                motor_state_to_motor_value(masked_sms[:,3]),**sa) 
        xlabel('right sensor');ylabel('right motor');
        xlim(0,1)
        ylim(-1,1)
        
        endplot(f'sms_habit_{int(habit_color)}',copy_to_summary=False)
        
    # ### light relative to robot
    # r2lx = np.array([l-r for l,r in zip(xs,lxs)]).ravel()
    # r2ly = np.array([l-r for l,r in zip(ys,lys)]).ravel()
    # r2lxs = [x * np.cos(a) - y * np.sin(a) for x,y,a in zip(r2lx,r2ly,alphas)]
    # r2lys = [x * np.sin(a) + y * np.cos(a) for x,y,a in zip(r2lx,r2ly,alphas)]

    # newplot()
    # scatter(r2lxs,r2lys,**scatter_args)
    # xlabel('u');ylabel('v');title("Light Relative To Robot");
    # plot(0.0,0.0,'kx')
    # xlim(-1,1);ylim(-1,1)
    # endplot('robot_centered',copy_to_summary=False)    
        
    ### robot position
    newplot()
    scatter(r_pos[:,0],r_pos[:,1],**scatter_args)
    #xlabel('x');ylabel('y');title("Robot Position");
    xticks([]);yticks([])
    xlim(0,1);ylim(0,1)
    endplot('robot_position')
        
    ### Motor States & Sensor States
    newplot(figsize=(10,9))

    subplot2grid((2,2),(0,0))
    scatter(sms[:,3],sms[:,2],**scatter_args) ## deliberately swapped left and right, 
    xlim(0,1);ylim(0,1)                       ## so to make mapping transition make sense
    xlabel('$\mu_r$');ylabel('$\mu_l$');title("Abstract Sensorimotor State");
    
    subplot2grid((2,2),(1,1))
    scatter(motor_state_to_motor_value(sms[:,2]),
            motor_state_to_motor_value(sms[:,3]),**scatter_args)
    xlim(-1,1);ylim(-1,1)
    xlabel('Left Motor');ylabel('Right Motor');title("Motor state");

    subplot2grid((2,2),(0,1))
    scatter(sms[:,0],sms[:,1],**scatter_args)
    xlim(0,1);ylim(0,1)
    xlabel('ls');ylabel('rs');title("Sensor state");

    ## mapping visualization
    subplot2grid((2,2),(1,0))
    ls = linspace(0,1,1001)
    # sms_lm,sms_rm = meshgrid(ls,ls)
    # g = 1.0-motor_state_to_motor_value(sms_lm)
    # b = 1.0-motor_state_to_motor_value(sms_rm)
    # r = np.ones_like(g)*0.0
    # imshow(np.dstack([r,g,b]),extent=(0,1,0,1),origin='lower')
    plot(ls,motor_state_to_motor_value(ls))
    xlim(0,1);ylim(-1.0,1.0);
    gca().set_aspect('auto')
    xlabel('SMS.xm');ylabel('xm');title("IDSM 'State' to Motor Map");
    
    endplot('sensorimotor_state')
