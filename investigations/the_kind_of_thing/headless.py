from pylab import *
from model import Model
import numpy as np
import os,sys,time

from tracker import TrackingManager

DATA_DIR = '/home/mde/Desktop/idsmtrials/'

start_time = time.time()
total_its_of_all_trials = 0

DEFAULT_DUR = int(250000)

def trial(trialID,DUR=DEFAULT_DUR):
    global total_its_of_all_trials
    tracker = TrackingManager()

    EVERY_ITERATION = lambda m: True
    FREQUENTLY = lambda m: (m.it % 100) == 0
    START = lambda m: m.it == 0
    END = lambda m: m.it == DUR-1

    tracker.track('idsm_parameters','m.idsm.parameters',START)
    tracker.track('robot_position','m.robot.pos',EVERY_ITERATION)
    tracker.track('light_position','[m.light.x,m.light.y]',EVERY_ITERATION)
    tracker.track('sample_times','m.t',EVERY_ITERATION)
    tracker.track('sample_its','m.it',EVERY_ITERATION)
    tracker.track('sensorimotor_state','m.idsm.SMS_H',EVERY_ITERATION)
    # tracker.track('closest_node_index','m.idsm.getNodeClosestToSMS()',EVERY_ITERATION)

    tracker.track('frequently_times','m.t',FREQUENTLY)
    tracker.track('frequently_its','m.it',FREQUENTLY)
    tracker.track('node_locs','m.idsm.all_nodes.locs',FREQUENTLY)
    tracker.track('node_weights','m.idsm.all_nodes.weights',FREQUENTLY)

    tracker.add_pickle_obj('motor_state_to_motor_value',Model.motor_state_to_motor_value)
    
    m = Model(headless=True,seed=trialID)

    for _ in range(DUR) :
        if (m.it % 100 == 0) :
            total_time = time.time()-start_time
            print(f'{m.it} / {DUR} \t ({100.0*m.it/DUR:2.2f}%) \t {(total_its_of_all_trials / (total_time)):0.2f} its/sec [total time: {total_time:0.0f} s]', sep=' ', end='\r', flush=True)
        tracker.iterate(m)
        m.iterate()
        total_its_of_all_trials += 1
    
    tracker.save(os.path.join(DATA_DIR,str(trialID)))

if __name__ == '__main__':
    N_TRIALS = 10
    for trial_index in range(0,N_TRIALS) :
        print()
        print(f'Starting trial {trial_index} of {N_TRIALS}')
        trial(trial_index)
    print()
    quit()
