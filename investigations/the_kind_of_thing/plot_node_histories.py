from pylab import *
import sys,os
import dill as pickle
from scipy.spatial.distance import cdist
from plotting_utils import *

fw = 8
fh = 5

closestppars = {
    'alpha': 0.8,
    'ms' : 0.3,
}

def plot_node_histories(trial_path,summary_path=None):
    trial_name = trial_path.rsplit('/', 1)[-1]
    vis_path = create_visualization_folder(trial_path)

    frequently_times = np.load(trial_path+'/frequently_times.npy',allow_pickle=True)
    sample_times = np.load(trial_path+'/sample_times.npy',allow_pickle=True)
    nodes = data_to_nodes(trial_path)

    color_data,color_data_f,cmap = segment_data_to_colors(trial_path)

    figure(figsize=(fw,fh))    
    dist = np.load(os.path.join(trial_path,'node_distance_history.npy'),allow_pickle=True)
    dist = dist.astype('float32') 
    no_node_mask = dist==-1.0 ## this is where there is no node
    dist[no_node_mask] = 1000000 ## where there is no node, make distance seem very very large

    threshold = 0.00001
    k_d = 500
    p = 2.0/(1.0+exp(k_d*dist**2)) > threshold

    closest_nodes_rows = np.argmin(dist,axis=0)    
    
    imshow(p,aspect='auto',cmap='gray_r',interpolation='none',
           extent=(0,frequently_times.max(),0,len(nodes)),origin='lower',alpha=0.75)

    ## highlight closest nodes
    scatter(frequently_times,closest_nodes_rows,c=color_data_f,cmap=cmap,s=0.3,alpha=0.8,vmin=0,vmax=10.0)#,'rs',**closestppars)

    ## super impose time series showing the closest node (in red)
    # plot(sample_times,closest_node,'r,')
    xlabel('time')
    ylabel('node index')
    xlim(0,max(sample_times))
    #colorbar()
    #plot(-1,1,'k.',label='$d(N_\\vec{p},\\vec{x}) > '+str(threshold)+')$')
    #legend(loc='lower right')
    title('$d(N_\\vec{p},\\vec{x}) > '+f'{threshold:0.0E})$')
    tight_layout()
    savefig(vis_path+f'/node_proximity.png',dpi=300)

    if summary_path != None :
        savefig(summary_path+'/'+f'node_proximity_{trial_name}.png',dpi=300)
    #show()
    close()


    figure(figsize=(fw,fh))    
    ### weight history plot
    weight_heatmap = np.zeros((len(frequently_times),len(nodes)),dtype=np.float32)
    for n_i,n in enumerate(nodes):
        b = n['b']
        d = n['d']
        weight_heatmap[b:d,n_i] = n['weight_h']

    ## show node histories (weights change over time)
    imshow(weight_heatmap.T,aspect='auto',cmap='gray_r',interpolation='none',
           extent=(0,frequently_times.max(),0,len(nodes)),origin='lower',alpha=0.75)
    ## highlight closest nodes
    #plot(frequently_times,closest_nodes_rows,'rs',**closestppars)
    scatter(frequently_times,closest_nodes_rows,c=color_data_f,cmap=cmap,s=0.3,alpha=0.8,vmin=0.0,vmax=10.0)#,'rs',**closestppars)
    ## super impose time series showing the closest node (in red)
    xlabel('time')
    ylabel('node index')
    xlim(0,max(sample_times))
    title('Node Weights')
    tight_layout()
    savefig(vis_path+f'/weight_history.png',dpi=300)

    if summary_path != None :
        savefig(summary_path+'/'+f'weight_history_{trial_name}.png',dpi=300)
    close()   

 
