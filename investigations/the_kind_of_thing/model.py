# from kivy.config import Config
# Config.set('kivy','log_level','info')
# # Config.set('kivy','log_enable', '0')
# Config.write()
# 
import pylab as pl
import numpy as np

from idsm.utils import logger
from idsm.node_data import Node
from idsm import SensorimotorDimension, IDSM
 
from robot import Robot,Light,Sound
import time

current_time = lambda: int(round(time.time() * 1000))


class Model(object):
    def __init__(self,headless=False,seed='random',*args,**kwargs) :
        self.headless = headless
        self.frames_per_draw = 1.0
        self.smds = [SensorimotorDimension('ls',(0.0,1.0)),
                     SensorimotorDimension('rs',(0.0,1.0)),
                     SensorimotorDimension('lm',(-1.0,1.0)),
                     SensorimotorDimension('rm',(-1.0,1.0)),]
        self.DT = 0.01
        self.idsm_parameter_overrides = {
            'N_NODES': 2**(13),
            'k_d': 500.0, # bigger = more nodes / SM-volume
            'k_omega': 1.0,
            'k_weight_degradation' : 10.0/2,
            'k_weight_reinforcement' : 2000.0,
            'random_motor_activity_threshold' : 1.0,
            'changes_in_random_direction_per_t' :  10.0,
            'init_random_scale' : 0.25,
            'd_random_scale' : 1.0*0.0,
            'k_activation_delay' : 1.0,
        }
        
        self.idsm = IDSM(self.smds,DT=self.DT,
                         parameter_overrides=self.idsm_parameter_overrides)

        self.robot = Robot()
        self.light = Light()
        #self.light = Sound()
        self.robot.lights.append(self.light)
        self.init(seed=seed)
        self.experiment_running = False

        self.randomness_history = np.ones(self.robot.history_length)
        
        super(Model, self).__init__(**kwargs)

        def iterate(arg) :
            self.iterate()

        if self.headless == False:
            self.frames_per_draw = 10.0
            from kivy.clock import Clock            
            Clock.schedule_interval(iterate, 1.0/60.0)

    def init(self,seed) :
        self.reset_time = current_time()
        self.sensor_dim_nodes_as_vectors = np.zeros((self.idsm.N_NODES*2,2))
        self.motor_dim_nodes_as_vectors  = np.zeros((self.idsm.N_NODES*2,2))
        self.alpha_for_node_vectors      = np.zeros((self.idsm.N_NODES*2,1))
        self.closest_node_pos = np.zeros(4)
        self.reset(seed)

    def reset(self,seed) :
        # print('---------------------')
        if seed == 'random' :
            seed = np.random.randint(100000)
        np.random.seed(seed)
        logger.info(f'Setting random seed to {seed}')
        self.robot.reset()
        self.robot.lm[:] = np.random.rand()*2.0 - 1.0
        self.robot.rm[:] = np.random.rand()*2.0 - 1.0

        self.experiment_running = 0
        self.robot.iterate()
        self.robot.it = 0

        self.idsm.reset()
        self.idsm.iteration = 0
        self.it = 0
        self.t = 0.0
        self.idsm.SMS_H[:] = np.array([self.robot.ls,
                                       self.robot.rs,
                                       0.0,0.0],dtype=np.float32)

    @staticmethod
    def motor_state_to_motor_value(ms) :
        ## OLD
        f = 4.0 ## initial freq
        a = 3   ## acceleration of frequency
        mv = np.sin((f+ms)*2.0*np.pi*ms**a)
        return mv
        # ## simple
        # mv = np.sin((1.0*ms)*2.0*np.pi*(1.0*ms)**2)
        # return mv

        # ## symmetrical (but bad b/c it unnecessarily duplicates)
        # freq = 1.0-(4.0*(ms-0.5)**2)
        # mv = np.cos((1.0-freq)*10.0*np.pi)
        # return mv

    def iterate(self) :
        ## simulate x times
        for _ in range(int(self.frames_per_draw)) :
            self.it+=1
            self.t = self.DT*self.it
            time = float(self.it) * self.DT
            self.idsm.iterate()
            self.idsm.all_nodes.DtoH()
            
            # closest_node_index = self.idsm.getNodeClosestToSMS()
            # if(closest_node_index is not None) :
            #     # print(closest_node_index)
            #     # print(self.idsm.all_nodes.locs[:,closest_node_index])
            #     self.closest_node_pos[:] = self.idsm.all_nodes.locs[:,closest_node_index]
                
            
            self.robot.lm[:] = Model.motor_state_to_motor_value(self.idsm.SMS_H[2])
            self.robot.rm[:] = Model.motor_state_to_motor_value(self.idsm.SMS_H[3])
            
            # circleling light, stopping once per cycle
            self.light.x[:] = 0.5 + 0.333*np.cos(0.1*time+np.sin(time*0.1)*1.0)
            self.light.y[:] = 0.5 + 0.333*np.sin(0.1*time+np.sin(time*0.1)*1.0)

            # # simple circling light
            # self.light.x[:] = 0.5 + 0.333*np.cos(time*0.1)
            # self.light.y[:] = 0.5 + 0.333*np.sin(time*0.1)
            
            # # back and forth light
            # self.light.x[:] = 0.5 + 0.333*+np.sin(time*0.1)
            # self.light.y[:] = 0.5

            # # stationary light
            # self.light.x[:] = 0.5
            # self.light.y[:] = 0.5

            # # no light
            # self.light.x[:] = -100.5
            # self.light.y[:] = -100.5

            # # moving target light
            # if np.sqrt((self.light.x - self.robot.pos[0])**2 +
            #         (self.light.y - self.robot.pos[1])**2) < 0.1 or (self.it % 1000 == 0) :
            #     self.light.x[:] = np.random.rand()
            #     self.light.y[:] = np.random.rand()

            
            self.robot.iterate()
            self.randomness_history[self.robot.it%self.robot.history_length] = self.idsm.random_amount[0]

            # ## update sensor state for IDSM
            self.idsm.SMS_H[0] = self.robot.ls
            self.idsm.SMS_H[1] = self.robot.rs
            self.idsm.SMS_H[2] = (self.idsm.SMS_H[2] + 1.0) % 1.0
            self.idsm.SMS_H[3] = (self.idsm.SMS_H[3] + 1.0) % 1.0

        self.prepare_visualization_data()

    def prepare_visualization_data(self) :
        if self.headless == False:
            # ## prepare visualization data    
            self.sensor_dim_nodes_as_vectors[::2,:]   = self.idsm.all_nodes.locs.T[:,:2]
            self.sensor_dim_nodes_as_vectors[1::2,:]  = (self.idsm.all_nodes.locs.T[:,:2]
                                                         + self.idsm.all_nodes.vels.T[:,:2]*0.01)

            self.motor_dim_nodes_as_vectors[::2,:]   = self.idsm.all_nodes.locs.T[:,2:]
            self.motor_dim_nodes_as_vectors[1::2,:]  = (self.idsm.all_nodes.locs.T[:,2:] +
                                                        self.idsm.all_nodes.vels.T[:,2:]*0.01)

            omegas = self.idsm.all_nodes.weights / 1000.0
            self.alpha_for_node_vectors[:,0] = np.vstack([omegas,omegas]).T.ravel()


