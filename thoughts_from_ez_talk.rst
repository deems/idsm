"potentialities"

chaotic itin. and IDSM

reread varela 2001

shadow of perception

transition between dimensionality from synchronous to asynchronous

external norms -- do you think that all norms take a similar form as
that underpinned by the "mother of all norms", viability?



can I model the piagetian cycle 24 mins in?

IDSM with non-SM dimensions as @internal@ state

multi idsm robot (one each wheel)

what motivates **networks** of activity? / what would a *single* habit be?

couldn't schemes be exclusively supported by "agent-side" support?


Any thoughts on what would cause the variety of zero-modes?

==================

thoughts from Pickering's talk

- the stability of the homeostat scales very poorly as the number of
  interconnected homeostats increases. Is this also the case for
  IDSMs?

- What about hierarchical homeostat networks. Each cluster of (say) 4
  can somehow decide to veto or not veto its relationship to the
  others. This could be decided by e.g. how many are currently
  randomizing?
